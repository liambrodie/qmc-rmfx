(* ::Package:: *)

(*Liam Brodie:
Comparing IUF and qmc - rmf P (mu_z) at different temps and xp to see if there is a phase transition at some mu_z.
See https://arxiv.org/abs/2304.07836 for mu_z definition*)


(* ::Input::Initialization:: *)
Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];


(* ::Input::Initialization:: *)
sigma$mass=491.5;(*meson masses used in IUF paper: arxiv: 1008.3030v1*)
omega$mass=782.5;
rho$mass=763.0;
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = fm3$baryon$saturation$density* hbarc^3;(*MeV^3*)

(*sigma$field$guess=40; (*initial choices for FindRoot*)
omega$field$guess=30;
rho$field$guess = 0.0001;
neutron$chemical$potential$guess = 900(*950*);
proton$chemical$potential$guess = 900(*950*);
electron$chemical$potential$guess=120(*120*);*)


(* ::Input::Initialization:: *)
temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)
xp$tab$impt=Import["iuf_data/hs_iuf_xp.dat"];
xp$tab=Table[xp$tab$impt[[i,1]],{i,1+2,Length[xp$tab$impt]}];(*to get rid of the header*)
(*change this if the HS() model using is changed*)
nb$fm3$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nb$tab=Table[nb$fm3$tab$impt[[i,1]],{i,1+2,Length[nb$fm3$tab$impt]}];(*to get rid of the header*)


(* ::Input::Initialization:: *)
(*imports p(mu_z) from a slice in the IUF EoS table*)
all$iuf$phases$muz$p$fun[temp$val_,xp$val_]:=Module[{iuf$neutron$mass,hbarc,impt$crust$tab,crust$muz$p$tab,impt$mixed$tab,impt$homo$tab,mix$muz$p$tab,homo$muz$p$tab,all$muz$p$tab,muz$p$tab},
iuf$neutron$mass=939.56535;(*mev*)
hbarc=197.3;(*mev fm*)
impt$crust$tab=Import["iuf_eos_thermo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_eos_thermo_iuf.dat"];

muz$p$tab=Table[{impt$crust$tab[[i,-1]],impt$crust$tab[[i,4]]*impt$crust$tab[[i,2]]*hbarc^3},{i,1,Length[impt$crust$tab]}]

]


(* ::Input::Initialization:: *)
(*generates p(mu_z) from the qmc-rmf mean-field solutions*)
rmf$p$of$muz$fun[temp$val_,xp$val_,rmf$model_]:=Module[{impt$rmf$mean$fields,bag$const$impt,p$of$muz$tab,i,rmf$mean$fields$one$xp,nbfm3$of$muz$tab},

impt$rmf$mean$fields=Import["qmc_rmf_mean_field_solns/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

(*this AppendTo loop may be unnecessary as the imported mean-field file is only for one xp*)
rmf$mean$fields$one$xp={};
For[i=1,i<=Length[impt$rmf$mean$fields],i++,
If[impt$rmf$mean$fields[[i,-1]]==xp$val,AppendTo[rmf$mean$fields$one$xp,impt$rmf$mean$fields[[i]]],None
]
];

bag$const$impt=Import["bag_consts_qmc_rmfs.dat"][[rmf$model]][[1]];

p$of$muz$tab=Table[{(1-rmf$mean$fields$one$xp[[i,16]])*rmf$mean$fields$one$xp[[i,11]]+rmf$mean$fields$one$xp[[i,16]]*(rmf$mean$fields$one$xp[[i,12]]+rmf$mean$fields$one$xp[[i,13]]),pressure$npe$photons[rmf$mean$fields$one$xp[[i,1]],rmf$mean$fields$one$xp[[i,2]],rmf$mean$fields$one$xp[[i,3]],rmf$mean$fields$one$xp[[i,4]],rmf$mean$fields$one$xp[[i,5]],rmf$mean$fields$one$xp[[i,6]],rmf$mean$fields$one$xp[[i,7]],rmf$mean$fields$one$xp[[i,8]],rmf$mean$fields$one$xp[[i,9]],rmf$mean$fields$one$xp[[i,10]],rmf$mean$fields$one$xp[[i,11]],rmf$mean$fields$one$xp[[i,12]],rmf$mean$fields$one$xp[[i,13]],rmf$mean$fields$one$xp[[i,15]]]+bag$const$impt},{i,1,Length[rmf$mean$fields$one$xp]}];

nbfm3$of$muz$tab=Table[{p$of$muz$tab[[i,1]],rmf$mean$fields$one$xp[[i,-3]]/hbarc^3},{i,1,Length[rmf$mean$fields$one$xp]}];

Export["nbfm3_of_muz_qmc_rmf_"<>ToString[rmf$model]<>"/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_nbfm3_of_muz.dat",nbfm3$of$muz$tab];

p$of$muz$tab
]


(* ::Input::Initialization:: *)
(*Finds when one phase is thermodynamically favored over another phase-- when IUF and QMC-RMF intersect in the p(mu_z) plane.
A crude FindRoot is done where the pressure difference is taken at each mu_z and the transition mu_z is where the sign 
in the pressure difference changes*)
comparison$fun[temp$val_,xp$val_,rmf$model_]:=Module[{crust$plt,rmf$plt,crust$interp,rmf$interp,crust$interp$plt,rmf$interp$plt,show$data$plt,show$interp$plt,find$root$fun,muz,muz$trans,diff$crust$rmf$fun,diff$tab,root$tab,i},

crust$interp=Interpolation[all$iuf$phases$muz$p$fun[temp$val,xp$val],InterpolationOrder->2];
rmf$interp=Interpolation[rmf$p$of$muz$fun[temp$val,xp$val,rmf$model],InterpolationOrder->2];

diff$tab=Table[{muz,(rmf$interp[muz]-crust$interp[muz])/(rmf$interp[muz]+crust$interp[muz])},{muz,rmf$p$of$muz$fun[temp$val,xp$val,rmf$model][[1,1]],1300(*rmf$p$of$muz$fun[temp$val,xp$val,rmf$model][[-1,1]]*),0.1}];

root$tab={};
For[i=2,i<=Length[diff$tab],i++,
If[diff$tab[[i-1,2]]<0&&diff$tab[[i,2]]>0,
AppendTo[root$tab,(diff$tab[[i-1,1]]+diff$tab[[i,1]])/2];
]
];
muz$trans=Min[root$tab];
If[Length[root$tab]==0,Print["WARNING: NO ROOT"]];

Export["muz_trans_qmc_rmf_"<>ToString[rmf$model]<>"/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_muz_trans_crust_core_pressure_val.dat",Chop[{muz$trans,crust$interp[muz$trans],rmf$interp[muz$trans]}]];

Print["Done with T="<>ToString[temp$val]<>", xp="<>ToString[xp$val]<>" comparing phases"];

{temp$val,xp$val,rmf$model,muz$trans,(rmf$interp[muz$trans]-crust$interp[muz$trans])}

]

(*t$start= 71 ;t$end= 81 ;*)

Do[Do[Do[comparison$fun[temp$tab[[t]],xp$tab[[xp]],model$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{model$num,1,4}]
