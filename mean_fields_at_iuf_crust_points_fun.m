(* ::Package:: *)

(* ::Input::Initialization:: *)
(*Liam Brodie 2022:
package to solve and save the mean fields found by varying baryon number density at fixed proton fraction and temperature*)

BeginPackage["solve$mean$fields$at$hs$iuf$crust$nb$xp`"];

Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];

(*publicly available functions*)
mf$solver;

Begin["`Private`"];


(* ::Input::Initialization:: *)
mf$solver[temperature$val_,xp$val_,rmf$set_]:=Module[{sigma$mass,omega$mass,rho$mass,nucleon$mass,electron$mass,hbarc,fm3$baryon$saturation$density,baryon$saturation$density,sigma$field$guess,omega$field$guess,rho$field$guess,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,empty$tab,impt$coups,parameter$set$index,sigma$coupling$var,omega$coupling$var,rho$coupling$var,b$var,c$var,a$zeta$var,big$lambda$var,bag$const,temp$tab$impt,temp$tab,impt$crust$tab,nb$fm3$xp$tab,nbfm3$xp$tab,i,j,nb$tab$impt,nbfm3$tab,initial$solver$soln,rmf$iterator,generate$initial$solver$soln},

(*Constants*)
sigma$mass=491.5;(*meson masses used in IUF paper: arxiv: 1008.3030v1*)
omega$mass=782.5;
rho$mass=763.0;
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = fm3$baryon$saturation$density* hbarc^3;(*MeV^3*)

(*sigma$field$guess=(*40*)40; (*initial choices for FindRoot*)
omega$field$guess=(*30*)30;
rho$field$guess = (*0.0001*)0.0001;
neutron$chemical$potential$guess = 900(*900*)(*950*);
proton$chemical$potential$guess = 860(*900*)(*950*);
electron$chemical$potential$guess=120(*120*)(*120*);*)

(*
sigma$field$guess=10^-7; (*initial choices for FindRoot*)
omega$field$guess=10^-7;
rho$field$guess = -10^-8;
neutron$chemical$potential$guess = 938(*950*);
proton$chemical$potential$guess = 937(*900*)(*950*);
electron$chemical$potential$guess=0.2(*120*);
*)
(*
sigma$field$guess=2.32025; (*initial choices for FindRoot good for t=0.1mev, xp=0.2*)
omega$field$guess=1.05685;
rho$field$guess = -0.415332;
neutron$chemical$potential$guess = 940.659(*950*);
proton$chemical$potential$guess = 931.458(*950*);
electron$chemical$potential$guess=78.1164(*120*);
*)

sigma$field$guess=2.32187; (*initial choices for FindRoot good for t=0.1mev, xp=0.49*)
omega$field$guess=1.0573995478137754`;
rho$field$guess = -0.0277141010221055`;
neutron$chemical$potential$guess = 936.5993067539985`(*950*);
proton$chemical$potential$guess = 935.9941737050688`(*950*);
electron$chemical$potential$guess=103.00844241469683`(*120*);

(*
sigma$field$guess=2.20171; (*initial choices for FindRoot good for t=36.3078mev, xp=0.30*)
omega$field$guess=1.05714;
rho$field$guess = -0.0277141010221055`;
neutron$chemical$potential$guess = 936.5993067539985`(*950*);
proton$chemical$potential$guess = 935.9941737050688`(*950*);
electron$chemical$potential$guess=103.00844241469683`(*120*);
*)
(*sigma$field$guess=2.20169; (*initial choices for FindRoot good for t=36.3078mev, xp=0.29*)
omega$field$guess=1.05711;
rho$field$guess = -0.300349;
neutron$chemical$potential$guess = 834.604(*950*);
proton$chemical$potential$guess = 798.351(*950*);
electron$chemical$potential$guess=44.0978(*120*);
*)

impt$coups=Import["qmc_rmf_coupling_list.hdf",{{"Datasets"},{"Dataset1"}}];
parameter$set$index=rmf$set;
sigma$coupling$var=impt$coups[[parameter$set$index,1]];
omega$coupling$var=impt$coups[[parameter$set$index,2]];
rho$coupling$var=impt$coups[[parameter$set$index,3]];
b$var=impt$coups[[parameter$set$index,4]];
c$var=impt$coups[[parameter$set$index,5]];
a$zeta$var=impt$coups[[parameter$set$index,6]];
big$lambda$var=impt$coups[[parameter$set$index,7]];

bag$const=Import["bag_consts_qmc_rmfs.dat"][[parameter$set$index]][[1]];

temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)
nb$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nbfm3$tab=Table[nb$tab$impt[[i,1]],{i,1+2,Length[nb$tab$impt]}];


nbfm3$xp$tab = Table[{nbfm3$tab[[i]], xp$val}, {i, 251, 306}];(*251-306 corresponds to nB approx 0.01-1.58 fm^-3*)

initial$solver$soln={mean$field$solver$gen$p$frac$save$fields[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling$var,omega$coupling$var,rho$coupling$var,b$var,c$var,a$zeta$var,big$lambda$var,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,nbfm3$xp$tab[[1,1]]*hbarc^3,temperature$val,nbfm3$xp$tab[[1,2]]]};


Print[initial$solver$soln];

rmf$iterator[i_] := Module[{},
(*If previous nB gridpoint soln is a "good soln" when plugged back into the eqs of motion,
use it as an initial guess to help FindRoot*)
   If[ToString[check$of$mean$field$solver$gen$p$frac[
      initial$solver$soln[[i - 1, 1]], 
      initial$solver$soln[[i - 1, 2]], 
      initial$solver$soln[[i - 1, 3]], sigma$coupling$var, 
      omega$coupling$var, rho$coupling$var, b$var, c$var, a$zeta$var, 
      big$lambda$var, initial$solver$soln[[i - 1, 11]], 
      initial$solver$soln[[i - 1, 12]], 
      initial$solver$soln[[i - 1, 13]], 
      nbfm3$xp$tab[[i - 1, 1]]*hbarc^3, temperature$val, 
      nbfm3$xp$tab[[i - 1, 2]]]] == "{Good Check}",

    mean$field$solver$gen$p$frac$save$fields[
     initial$solver$soln[[i - 1, 1]], initial$solver$soln[[i - 1, 2]],
      initial$solver$soln[[i - 1, 3]], sigma$coupling$var, 
     omega$coupling$var, rho$coupling$var, b$var, c$var, a$zeta$var, 
     big$lambda$var, initial$solver$soln[[i - 1, 11]], 
     initial$solver$soln[[i - 1, 12]], 
     initial$solver$soln[[i - 1, 13]], nbfm3$xp$tab[[i, 1]]*hbarc^3, 
     temperature$val, nbfm3$xp$tab[[i, 2]]],

(*if using the previous nB gridpoint soln as an initial guess does not help FindRoot 
solve the mean fields, use the soln of the first density point in the grid as an initial guess*)
 mean$field$solver$gen$p$frac$save$fields[
     initial$solver$soln[[1, 1]], initial$solver$soln[[1, 2]],
      initial$solver$soln[[1, 3]], sigma$coupling$var,
     omega$coupling$var, rho$coupling$var, b$var, c$var, a$zeta$var,
     big$lambda$var, initial$solver$soln[[1, 11]],
     initial$solver$soln[[1, 12]],
     initial$solver$soln[[1, 13]], nbfm3$xp$tab[[i, 1]]*hbarc^3,
     temperature$val, nbfm3$xp$tab[[i, 2]]]
    ](*uses soln of first density point as initial guess*)
   ];

Do[

AppendTo[initial$solver$soln,rmf$iterator[i]],{i,2,Length[nbfm3$xp$tab]}];

(*if a soln is not a "good soln" (see above) at an nB grid point, 
use the soln of the first density point in the grid as an initial guess.
this part may be redundant with the above rmf$iterator function*)
For[j=1,j<=Length[initial$solver$soln],j++,
If[ToString[check$of$mean$field$solver$gen$p$frac[
      initial$solver$soln[[j, 1]],
      initial$solver$soln[[j, 2]],
      initial$solver$soln[[j, 3]], sigma$coupling$var,
      omega$coupling$var, rho$coupling$var, b$var, c$var, a$zeta$var,
      big$lambda$var, initial$solver$soln[[j, 11]],
      initial$solver$soln[[j, 12]],
      initial$solver$soln[[j, 13]],
      nbfm3$xp$tab[[j, 1]]*hbarc^3, temperature$val,
      nbfm3$xp$tab[[j, 2]]]] != "{Good Check}",
	
	initial$solver$soln=ReplacePart[j->mean$field$solver$gen$p$frac$save$fields[
     initial$solver$soln[[1, 1]], initial$solver$soln[[1, 2]],
      initial$solver$soln[[1, 3]], sigma$coupling$var,
     omega$coupling$var, rho$coupling$var, b$var, c$var, a$zeta$var,
     big$lambda$var, initial$solver$soln[[1, 11]],
     initial$solver$soln[[1, 12]],
     initial$solver$soln[[1, 13]], nbfm3$xp$tab[[j, 1]]*hbarc^3,
     temperature$val, nbfm3$xp$tab[[j, 2]]]][initial$solver$soln];Print["Replaced j= "<>ToString[j]<>""];
]
];


Export["qmc_rmf_mean_field_solns/t_"<>ToString[temperature$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[parameter$set$index]<>".dat",initial$solver$soln];
];


End[]; (* end of private part of module *)

EndPackage[];
