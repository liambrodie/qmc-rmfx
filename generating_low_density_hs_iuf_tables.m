(* ::Package:: *)

(* ::Input::Initialization:: *)
Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];


(* ::Input::Initialization:: *)
sigma$mass=491.5;(*meson masses used in IUF paper: arxiv: 1008.3030v1*)
omega$mass=782.5;
rho$mass=763.0;
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = fm3$baryon$saturation$density* hbarc^3;(*MeV^3*)

sigma$field$guess=40; (*initial choices for FindRoot*)
omega$field$guess=30;
rho$field$guess = 0.0001;
neutron$chemical$potential$guess = 900(*950*);
proton$chemical$potential$guess = 900(*950*);
electron$chemical$potential$guess=120(*120*);


(* ::Input::Initialization:: *)
temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)xp$tab$impt=Import["iuf_data/hs_iuf_xp.dat"];
xp$tab=Table[xp$tab$impt[[i,1]],{i,1+2,Length[xp$tab$impt]}];(*to get rid of the header*)
nb$fm3$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nb$tab=Table[nb$fm3$tab$impt[[i,1]],{i,1+2,Length[nb$fm3$tab$impt]}];(*to get rid of the header*)



(* ::Subsubsection::Initialization:: *)
(*(*Cutting low-density phase off at the mu_z transition point*)*)


(* ::Input::Initialization:: *)
(*import low-density phase, find what row corresponds to mu_z less than muz_trans. Then make a new, shorter table for all values less that that point. Rearrange table as a function of density and save*)



(* ::Input::Initialization:: *)
(*shortening eos.thermo file*)
low$density$eos$thermo$table$cutoff$fun[temp$val_,xp$val_,rmf$model_]:=Module[{iuf$neutron$mass,hbarc,impt$crust$tab,crust$muz$p$tab,impt$mixed$tab,impt$homo$tab,mix$muz$p$tab,homo$muz$p$tab,all$muz$p$tab,muz$p$tab,muz$trans$val,low$density$table,mass$rescaled$low$density$table,qmc$rmf$neutron$mass},

iuf$neutron$mass=939.56535;(*mev*)
qmc$rmf$neutron$mass=939;(*mev*)
hbarc=197.3;(*mev fm*)
impt$crust$tab=Import["iuf_eos_thermo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_eos_thermo_iuf.dat"];

muz$trans$val=Import["muz_trans_qmc_rmf_"<>ToString[rmf$model]<>"/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_muz_trans_crust_core_pressure_val.dat"][[1,1]];

(*select all rows corresponding to mu_z < mu_z transition*)
low$density$table=Select[impt$crust$tab,#1[[-1]]<muz$trans$val&];

(*neutron mass used in the HS(IUF) CompOSE EoS table as an offset is 939.56535 MeV, which is not used when solving the 
RMF eqs of motion for IUF or QMC-RMF so we change it to the used value of 939 MeV*)
mass$rescaled$low$density$table=Table[{Position[temp$tab,low$density$table[[i,1]]][[1,1]],Position[nb$tab,low$density$table[[i,2]]][[1,1]],Position[xp$tab,low$density$table[[i,3]]][[1,1]],low$density$table[[i,4]],low$density$table[[i,5]],((low$density$table[[i,6]]+1)*iuf$neutron$mass)/qmc$rmf$neutron$mass-1,low$density$table[[i,7]]*iuf$neutron$mass/qmc$rmf$neutron$mass,low$density$table[[i,8]]*iuf$neutron$mass/qmc$rmf$neutron$mass,(low$density$table[[i,9]]+1)*(low$density$table[[i,2]]*iuf$neutron$mass)/(low$density$table[[i,2]]*qmc$rmf$neutron$mass)-1,(low$density$table[[i,10]]+1)*(low$density$table[[i,2]]*iuf$neutron$mass)/(low$density$table[[i,2]]*qmc$rmf$neutron$mass)-1,(*one additional quantity*)1,low$density$table[[i,12]]},{i,1,Length[low$density$table]}];

Export["hs_iuf_low_density_eos_thermo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_mass_rescaled_low_density_iuf_thermo_table_for_qmc_rmf_"<>ToString[rmf$model]<>".dat",mass$rescaled$low$density$table];

Print["Done with eos_thermo t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>""];

]


(* ::Input::Initialization:: *)
(*low$density$eos$thermo$table$cutoff$fun[temp$tab[[49]],xp$tab[[13]],1]*)
(*Be aware: the HS(IUF) EoS table is thermodynamically inconsistent in the t$index=49 and xp$index=13 slice.*)


(* ::Input::Initialization:: *)
Do[Do[Do[low$density$eos$thermo$table$cutoff$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
(*shortening eos.compo file*)
low$density$eos$compo$cutoff$fun[temp$val_,xp$val_,rmf$model_]:=Module[{impt$eos$compo,low$density$eos$compo$tab,impt$low$density$eos$thermo$highest$nbfm3,index$of$nbfm3$tab,upper$density$index,index$of$xp$tab,xp$index},
impt$eos$compo=Import["iuf_eos_compo/t_"<>ToString[temp$val]<>"_mev_eos_compo_iuf.dat"];
impt$low$density$eos$thermo$highest$nbfm3=Import["hs_iuf_low_density_eos_thermo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_mass_rescaled_low_density_iuf_thermo_table_for_qmc_rmf_"<>ToString[rmf$model]<>".dat"][[-1,2]];

index$of$nbfm3$tab=Table[{nb$tab[[i]],i},{i,1,Length[nb$tab]}];
index$of$xp$tab=Table[{xp$tab[[i]],i},{i,1,Length[xp$tab]}];
xp$index=Position[index$of$xp$tab,xp$val][[1,1]];

upper$density$index=Position[index$of$nbfm3$tab,impt$low$density$eos$thermo$highest$nbfm3][[1,1]];
low$density$eos$compo$tab=Select[impt$eos$compo,#1[[3]]==xp$index&&#1[[2]]<=upper$density$index&];

Export["hs_iuf_low_density_eos_compo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_low_density_iuf_eos_compo_for_qmc_rmf_"<>ToString[rmf$model]<>".dat",low$density$eos$compo$tab];

Print["Done with eos_compo t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>""]

]


(* ::Input::Initialization:: *)
Do[Do[Do[low$density$eos$compo$cutoff$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
impt$eos$micro=Import["iuf_eos_micro/eos_micro.dat"];


(* ::Input::Initialization:: *)
low$density$eos$micro$cutoff$fun[temp$val_,xp$val_,rmf$model_]:=Module[{(*impt$eos$micro,*)low$density$eos$micro$tab,impt$low$density$eos$thermo$highest$nbfm3,index$of$nbfm3$tab,upper$density$index,index$of$xp$tab,xp$index,index$of$t$tab,t$index,single$temp$eos$micro$tab,iuf$neutron$mass,iuf$proton$mass,qmc$rmf$neutron$mass,qmc$rmf$proton$mass,mass$rescaled$tab},

impt$low$density$eos$thermo$highest$nbfm3=Import["hs_iuf_low_density_eos_thermo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_mass_rescaled_low_density_iuf_thermo_table_for_qmc_rmf_"<>ToString[rmf$model]<>".dat"][[-1,2]];

iuf$neutron$mass=939.56535;(*mev*)
iuf$proton$mass=938.27201;(*mev*)
qmc$rmf$neutron$mass=939;(*mev*)
qmc$rmf$proton$mass=939;(*mev*)

index$of$nbfm3$tab=Table[{nb$tab[[i]],i},{i,1,Length[nb$tab]}];
index$of$xp$tab=Table[{xp$tab[[i]],i},{i,1,Length[xp$tab]}];
index$of$t$tab=Table[{temp$tab[[i]],i},{i,1,Length[temp$tab]}];
xp$index=Position[index$of$xp$tab,xp$val][[1,1]];
t$index=Position[index$of$t$tab,temp$val][[1,1]];

upper$density$index=Position[index$of$nbfm3$tab,impt$low$density$eos$thermo$highest$nbfm3][[1,1]];
low$density$eos$micro$tab=Select[(*single$temp$eos$micro*)Select[impt$eos$micro,#1[[1]]==t$index&],#1[[3]]==xp$index&&#1[[2]]<=upper$density$index&];

mass$rescaled$tab=Table[{low$density$eos$micro$tab[[i,1]],low$density$eos$micro$tab[[i,2]],low$density$eos$micro$tab[[i,3]],low$density$eos$micro$tab[[i,4]],low$density$eos$micro$tab[[i,5]],low$density$eos$micro$tab[[i,6]]*iuf$neutron$mass/qmc$rmf$neutron$mass,low$density$eos$micro$tab[[i,7]],low$density$eos$micro$tab[[i,8]]*iuf$proton$mass/qmc$rmf$proton$mass},{i,1,Length[low$density$eos$micro$tab]}];

Export["hs_iuf_low_density_eos_micro/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_low_density_iuf_eos_micro_for_qmc_rmf_"<>ToString[rmf$model]<>".dat",mass$rescaled$tab];

Print["Done with eos_micro t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>" gen. low-density EoS tab."]

]


(* ::Input::Initialization:: *)
(*t$start= 1 ;t$end= 81 ;*)

Do[Do[Do[low$density$eos$micro$cutoff$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


