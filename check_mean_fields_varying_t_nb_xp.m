(* ::Package:: *)

(*Liam Brodie
checking the mean fields solved for at fixed temperature and proton fraction and saves a file if the solutions are not "good"*)

BeginPackage["check$mean$fields$varying$t$nb$xp`"];



(* ::Input::Initialization:: *)
Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];

temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)
xp$tab$impt=Import["iuf_data/hs_iuf_xp.dat"];
xp$tab=Table[xp$tab$impt[[i,1]],{i,1+2,Length[xp$tab$impt]}];
nb$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nbfm3$tab=Table[nb$tab$impt[[i,1]],{i,1+2,Length[nb$tab$impt]}];(*to get rid of the header*)

rmf$check$soln$fun[temp$val_, xp$val_, rmf$model_] := 
 Module[{sigma$coupling$var, omega$coupling$var, rho$coupling$var, 
   b$var, c$var, a$zeta$var, big$lambda$var, tab$for$check$impt, 
   check$tab, i, bad$soln$list},

  tab$for$check$impt = 
   Import["qmc_rmf_mean_field_solns/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

  bad$soln$list = {};
  
  For[i = 1, i <= Length[tab$for$check$impt], i++,
   If[ToString[check$of$mean$field$solver$gen$p$frac[
      tab$for$check$impt[[i, 1]], tab$for$check$impt[[i, 2]], 
      tab$for$check$impt[[i, 3]], tab$for$check$impt[[i, 4]], 
      tab$for$check$impt[[i, 5]], tab$for$check$impt[[i, 6]], 
      tab$for$check$impt[[i, 7]], tab$for$check$impt[[i, 8]], 
      tab$for$check$impt[[i, 9]], tab$for$check$impt[[i, 10]], 
      tab$for$check$impt[[i, 11]], tab$for$check$impt[[i, 12]], 
      tab$for$check$impt[[i, 13]], tab$for$check$impt[[i, 14]], 
      tab$for$check$impt[[i, 15]], tab$for$check$impt[[i, 16]]]] != 
     "{Good Check}",
 
    AppendTo[
     bad$soln$list, {temp$val, xp$val, rmf$model, 
      tab$for$check$impt[[i, 14]]/197.3^3,check$of$mean$field$solver$gen$p$frac[
      tab$for$check$impt[[i, 1]], tab$for$check$impt[[i, 2]],
      tab$for$check$impt[[i, 3]], tab$for$check$impt[[i, 4]],
      tab$for$check$impt[[i, 5]], tab$for$check$impt[[i, 6]],
      tab$for$check$impt[[i, 7]], tab$for$check$impt[[i, 8]],
      tab$for$check$impt[[i, 9]], tab$for$check$impt[[i, 10]],
      tab$for$check$impt[[i, 11]], tab$for$check$impt[[i, 12]],
      tab$for$check$impt[[i, 13]], tab$for$check$impt[[i, 14]],
      tab$for$check$impt[[i, 15]], tab$for$check$impt[[i, 16]]]}]]
   ];
  Print["Done with T = " <> ToString[temp$val] <> " MeV and xp = " <> 
    ToString[xp$val] <> " for QMC-RMF" <> ToString[rmf$model] <> ""];

 If[bad$soln$list!={},Print["bad$soln$list"]; Print[bad$soln$list];
  
Export["bad_rmf_solns/t_"<>ToString[temp$val]<>"_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_bad_soln_list.dat",bad$soln$list];];
];

Do[Do[Do[rmf$check$soln$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]

EndPackage[];
