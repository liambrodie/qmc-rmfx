(* ::Package:: *)

(*Liam Brodie 2023*)


(* ::Subsection:: *)
(*Merging both phases in eos.thermo for each T and xp, combining all xp at each T, and combining all T *)


sigma$mass=491.5;(*meson masses used in IUF paper: arxiv: 1008.3030v1*)
omega$mass=782.5;
rho$mass=763.0;
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = 0.16 * hbarc^3;(*MeV^3*)

temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)
xp$tab$impt=Import["iuf_data/hs_iuf_xp.dat"];
xp$tab=Table[xp$tab$impt[[i,1]],{i,1+2,Length[xp$tab$impt]}];(*to get rid of the header*)
(*change this if the HS() model using is changed*)
nb$fm3$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nb$tab=Table[nb$fm3$tab$impt[[i,1]],{i,1+2,Length[nb$fm3$tab$impt]}];(*to get rid of the header*)

(*t$start= 1 ;t$end= Length[temp$tab] ;*)



(* ::Input::Initialization:: *)
(*merge phases above and below mu_z transition. if there is a density phase between phases in the HS(IUF) density grid,
add a mixed phase by performing a volume average of quantities*)
merge$low$mixed$and$high$density$eos$thermo$tables$fun[temp$val_,xp$val_,rmf$model_]:=Module[{x,impt$single$t$xp$eos$thermo$tab,impt$low$density$eos$thermo,impt$high$density$eos$thermo,density$gap$boundaries,nb$of$muz$crust$tab,nb$of$muz$core$tab,nb$of$muz$tab,muz$of$nb$tab,muz$of$nb$fun,density$gap$tab,p$of$nb$crust$tab,p$of$nb$core$tab,p$of$nb$plt,p$of$nb$mixed$tab,p$over$nb$mixed$tab,eps$of$nb$crust$tab,eps$of$nb$core$tab,eps$of$nb$mixed$tab,scaled$shifted$eps$mixed$tab,s$per$b$of$nb$mixed$tab,s$of$nb$crust$tab,s$of$nb$core$tab,s$of$nb$mixed$tab,s$per$b$mixed$tab,mun$of$nb$crust$tab,mun$of$nb$core$tab,mun$of$nb$mixed$tab,scaled$shifted$mun$mixed$tab,muq$of$nb$crust$tab,muq$of$nb$core$tab,muq$of$nb$mixed$tab,scaled$muq$mixed$tab,mul$of$nb$crust$tab,mul$of$nb$core$tab,mul$of$nb$mixed$tab,scaled$mul$mixed$tab,f$of$nb$crust$tab,f$of$nb$core$tab,f$of$nb$mixed$tab,scaled$shifted$f$mixed$tab,mixed$phase$eos$thermo$tab,eos$thermo},

impt$low$density$eos$thermo=Import["hs_iuf_low_density_eos_thermo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_mass_rescaled_low_density_iuf_thermo_table_for_qmc_rmf_"<>ToString[rmf$model]<>".dat"];
impt$high$density$eos$thermo=Import["high_density_eos_thermo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_high_density_eos_thermo.dat"];

nb$of$muz$crust$tab=Table[{impt$low$density$eos$thermo[[i,-1]],nb$tab[[impt$low$density$eos$thermo[[i,2]]]]},{i,1,Length[impt$low$density$eos$thermo]}];
nb$of$muz$core$tab=Table[{impt$high$density$eos$thermo[[i,-1]],nb$tab[[impt$high$density$eos$thermo[[i,2]]]]},{i,1,Length[impt$high$density$eos$thermo]}];
nb$of$muz$tab=Join[nb$of$muz$crust$tab,nb$of$muz$core$tab];
muz$of$nb$tab=Table[{nb$of$muz$tab[[i,2]],nb$of$muz$tab[[i,1]]},{i,1,Length[nb$of$muz$tab]}];
muz$of$nb$fun=Interpolation[muz$of$nb$tab,InterpolationOrder->1];

density$gap$boundaries={};
If[impt$high$density$eos$thermo[[1,2]]-impt$low$density$eos$thermo[[-1,2]]>1,
AppendTo[density$gap$boundaries,{impt$low$density$eos$thermo[[-1,2]],impt$high$density$eos$thermo[[1,2]]}]];

If[Length[density$gap$boundaries]==0,eos$thermo=Join[impt$low$density$eos$thermo,impt$high$density$eos$thermo],

density$gap$tab=Table[i,{i,density$gap$boundaries[[1,1]]+1,density$gap$boundaries[[1,2]]-1}];

If[Length[density$gap$boundaries]==0,Print["No density gap!"];Print[{temp$val,xp$val,rmf$model}];(*Abort[];*)];
density$gap$tab=Table[i,{i,density$gap$boundaries[[1,1]]+1,density$gap$boundaries[[1,2]]-1}];

p$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],impt$low$density$eos$thermo[[i,4]]*nb$tab[[impt$low$density$eos$thermo[[i,2]]]]*197.3^3},{i,1,Length[impt$low$density$eos$thermo]}];
p$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],impt$high$density$eos$thermo[[i,4]]*nb$tab[[impt$high$density$eos$thermo[[i,2]]]]*197.3^3},{i,1,Length[impt$high$density$eos$thermo]}];

p$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[p$of$nb$crust$tab,p$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
p$over$nb$mixed$tab=Table[p$of$nb$mixed$tab[[i,2]]/(p$of$nb$mixed$tab[[i,1]]*hbarc^3),{i,1,Length[p$of$nb$mixed$tab]}];


eps$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],(impt$low$density$eos$thermo[[i,10]]+1)*nb$tab[[impt$low$density$eos$thermo[[i,2]]]]*197.3^3*nucleon$mass},{i,1,Length[impt$low$density$eos$thermo]}];
eps$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],(impt$high$density$eos$thermo[[i,10]]+1)*nb$tab[[impt$high$density$eos$thermo[[i,2]]]]*197.3^3*nucleon$mass},{i,1,Length[impt$high$density$eos$thermo]}];

eps$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[eps$of$nb$crust$tab,eps$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
scaled$shifted$eps$mixed$tab=Table[eps$of$nb$mixed$tab[[i,2]]/(eps$of$nb$mixed$tab[[i,1]]*hbarc^3*nucleon$mass)-1,{i,1,Length[eps$of$nb$mixed$tab]}];

s$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],impt$low$density$eos$thermo[[i,5]]*nb$tab[[impt$low$density$eos$thermo[[i,2]]]]*197.3^3},{i,1,Length[impt$low$density$eos$thermo]}];
s$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],impt$high$density$eos$thermo[[i,5]]*nb$tab[[impt$high$density$eos$thermo[[i,2]]]]*197.3^3},{i,1,Length[impt$high$density$eos$thermo]}];

s$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[s$of$nb$crust$tab,s$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];

s$per$b$mixed$tab=Table[s$of$nb$mixed$tab[[i,2]]/(s$of$nb$mixed$tab[[i,1]]*hbarc^3),{i,1,Length[s$of$nb$mixed$tab]}];

mun$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],(impt$low$density$eos$thermo[[i,6]]+1)*nucleon$mass},{i,1,Length[impt$low$density$eos$thermo]}];
mun$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],(impt$high$density$eos$thermo[[i,6]]+1)*nucleon$mass},{i,1,Length[impt$high$density$eos$thermo]}];

mun$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[mun$of$nb$crust$tab,mun$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
scaled$shifted$mun$mixed$tab=Table[mun$of$nb$mixed$tab[[i,2]]/nucleon$mass-1,{i,1,Length[mun$of$nb$mixed$tab]}];

muq$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],(impt$low$density$eos$thermo[[i,7]])*nucleon$mass},{i,1,Length[impt$low$density$eos$thermo]}];
muq$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],(impt$high$density$eos$thermo[[i,7]])*nucleon$mass},{i,1,Length[impt$high$density$eos$thermo]}];

muq$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[muq$of$nb$crust$tab,muq$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
scaled$muq$mixed$tab=Table[muq$of$nb$mixed$tab[[i,2]]/nucleon$mass,{i,1,Length[muq$of$nb$mixed$tab]}];

mul$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],(impt$low$density$eos$thermo[[i,8]])*nucleon$mass},{i,1,Length[impt$low$density$eos$thermo]}];
mul$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],(impt$high$density$eos$thermo[[i,8]])*nucleon$mass},{i,1,Length[impt$high$density$eos$thermo]}];

mul$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[mul$of$nb$crust$tab,mul$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
scaled$mul$mixed$tab=Table[mul$of$nb$mixed$tab[[i,2]]/nucleon$mass,{i,1,Length[mul$of$nb$mixed$tab]}];

f$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$thermo[[i,2]]]],(impt$low$density$eos$thermo[[i,9]]+1)*nb$tab[[impt$low$density$eos$thermo[[i,2]]]]*197.3^3*nucleon$mass},{i,1,Length[impt$low$density$eos$thermo]}];
f$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$thermo[[i,2]]]],(impt$high$density$eos$thermo[[i,9]]+1)*nb$tab[[impt$high$density$eos$thermo[[i,2]]]]*197.3^3*nucleon$mass},{i,1,Length[impt$high$density$eos$thermo]}];

f$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[f$of$nb$crust$tab,f$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
scaled$shifted$f$mixed$tab=Table[f$of$nb$mixed$tab[[i,2]]/(f$of$nb$mixed$tab[[i,1]]*hbarc^3*nucleon$mass)-1,{i,1,Length[f$of$nb$mixed$tab]}];

mixed$phase$eos$thermo$tab=Table[{impt$low$density$eos$thermo[[i,1]],density$gap$tab[[i]],impt$low$density$eos$thermo[[i,3]],p$over$nb$mixed$tab[[i]],s$per$b$mixed$tab[[i]],scaled$shifted$mun$mixed$tab[[i]],scaled$muq$mixed$tab[[i]],scaled$mul$mixed$tab[[i]],scaled$shifted$f$mixed$tab[[i]],scaled$shifted$eps$mixed$tab[[i]],1,muz$of$nb$fun[nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];

eos$thermo=Join[impt$low$density$eos$thermo,mixed$phase$eos$thermo$tab,impt$high$density$eos$thermo];

];

Export["combined_single_t_xp_eos_thermo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_eos_thermo.dat",eos$thermo];

];


(* ::Input::Initialization:: *)
Do[Do[Do[merge$low$mixed$and$high$density$eos$thermo$tables$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
combine$diff$xp$eos$thermo$fun[temp$val_,rmf$model_]:=Module[{diff$xp$tab},

diff$xp$tab=Flatten[Table[Import["combined_single_t_xp_eos_thermo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$tab[[xp]]]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_eos_thermo.dat"],{xp,1,Length[xp$tab]}],1];

Export["combined_single_t_xp_eos_thermo_files/t_"<>ToString[temp$val]<>"_mev_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_thermo.dat",diff$xp$tab];

Print["Done with T = "<>ToString[temp$val]<>" MeV for QMC-RMF"<>ToString[rmf$model]<>" combine t xp eos thermo"];
]


(* ::Input::Initialization:: *)
Do[Do[combine$diff$xp$eos$thermo$fun[temp$tab[[t]],rmf$num],{t,1,Length[temp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
combine$diff$t$eos$thermo$fun[rmf$model_]:=Module[{diff$t$tab},

diff$t$tab=Flatten[Table[Import["combined_single_t_xp_eos_thermo_files/t_"<>ToString[temp$tab[[t]]]<>"_mev_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_thermo.dat"],{t,1,Length[temp$tab]}],1];

Export["combined_final_eos_thermo_files/all_t_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_thermo.dat",diff$t$tab];

Print["Done making eos.thermo for QMC-RMF"<>ToString[rmf$model]<>""];
]


(* ::Input::Initialization:: *)
Do[combine$diff$t$eos$thermo$fun[rmf$num],{rmf$num,1,4}]


(* ::Subsection::Initialization:: *)
(*Combining eos.micro*)


(* ::Input::Initialization:: *)
merge$low$mixed$and$high$density$eos$micro$tables$fun[temp$val_,xp$val_,rmf$model_]:=Module[{impt$low$density$eos$micro,impt$high$density$eos$micro,eos$micro,impt$single$t$xp$eos$micro$tab,density$gap$boundaries,i,density$gap$tab,m$star$over$m$of$nb$crust$tab,m$star$over$m$of$nb$core$tab,m$star$over$m$of$nb$mixed$tab,m$star$over$m$mixed$tab,mixed$eos$micro$tab},

impt$low$density$eos$micro=Import["hs_iuf_low_density_eos_micro/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_low_density_iuf_eos_micro_for_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

impt$high$density$eos$micro=Import["high_density_eos_micro_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_high_density_eos_micro.dat"];

density$gap$boundaries={};
If[impt$high$density$eos$micro[[1,2]]-impt$low$density$eos$micro[[-1,2]]>1,
AppendTo[density$gap$boundaries,{impt$low$density$eos$micro[[-1,2]],impt$high$density$eos$micro[[1,2]]}]];

If[Length[density$gap$boundaries]==0,eos$micro=Join[impt$low$density$eos$micro,impt$high$density$eos$micro],

density$gap$tab=Table[i,{i,density$gap$boundaries[[1,1]]+1,density$gap$boundaries[[1,2]]-1}];

m$star$over$m$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$micro[[i,2]]]],impt$low$density$eos$micro[[i,6]]},{i,1,Length[impt$low$density$eos$micro]}];
m$star$over$m$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$micro[[i,2]]]],impt$high$density$eos$micro[[i,6]]},{i,1,Length[impt$high$density$eos$micro]}];

m$star$over$m$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[m$star$over$m$of$nb$crust$tab,m$star$over$m$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
m$star$over$m$mixed$tab=Table[m$star$over$m$of$nb$mixed$tab[[i,2]],{i,1,Length[m$star$over$m$of$nb$mixed$tab]}];

mixed$eos$micro$tab=Table[{impt$low$density$eos$micro[[i,1]],density$gap$tab[[i]],impt$low$density$eos$micro[[i,3]],2,impt$low$density$eos$micro[[i,5]],m$star$over$m$mixed$tab[[i]],impt$low$density$eos$micro[[i,7]],m$star$over$m$mixed$tab[[i]]},{i,1,Length[density$gap$tab]}];

eos$micro=Join[impt$low$density$eos$micro,mixed$eos$micro$tab,impt$high$density$eos$micro];

];

Export["combined_single_t_xp_eos_micro_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_eos_micro.dat",eos$micro];
]


(* ::Input::Initialization:: *)
Do[Do[Do[merge$low$mixed$and$high$density$eos$micro$tables$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num], {t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
combine$diff$xp$eos$micro$fun[temp$val_,rmf$model_]:=Module[{diff$xp$tab},

diff$xp$tab=Flatten[Table[Import["combined_single_t_xp_eos_micro_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$tab[[xp]]]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_eos_micro.dat"],{xp,1,Length[xp$tab]}],1];

Export["combined_single_t_xp_eos_micro_files/t_"<>ToString[temp$val]<>"_mev_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_micro.dat",diff$xp$tab];

Print["Done with T = "<>ToString[temp$val]<>" MeV for QMC-RMF"<>ToString[rmf$model]<>" combine t xp eos.micro"];
]


(* ::Input::Initialization:: *)
Do[Do[combine$diff$xp$eos$micro$fun[temp$tab[[t]],rmf$num],{t,1,Length[temp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
combine$diff$t$eos$micro$fun[rmf$model_]:=Module[{diff$t$tab},

diff$t$tab=Flatten[Table[Import["combined_single_t_xp_eos_micro_files/t_"<>ToString[temp$tab[[t]]]<>"_mev_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_micro.dat"],{t,1,Length[temp$tab]}],1];

Export["combined_final_eos_micro_files/all_t_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_micro.dat",diff$t$tab];

Print["Done making eos.micro for QMC-RMF"<>ToString[rmf$model]<>""];
]


(* ::Input::Initialization:: *)
Do[combine$diff$t$eos$micro$fun[rmf$num],{rmf$num,1,4}]


(* ::Subsubsection::Initialization:: *)
(*Combining eos.compo*)


(* ::Input::Initialization:: *)
change$phase$index$fun[temp$val_,xp$val_,rmf$model_]:=Module[{impt$low$density$eos$compo,impt$high$density$eos$compo,switch$low$density$phase$fun,switch$high$density$phase$fun,new$low$density$eos$compo,new$high$density$eos$compo},
impt$low$density$eos$compo=Import["hs_iuf_low_density_eos_compo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_low_density_iuf_eos_compo_for_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

impt$high$density$eos$compo=Import["high_density_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_high_density_eos_compo.dat"];

(*makes sure correct phase index is used*)
switch$low$density$phase$fun[phase$val_]:=If[phase$val==1||phase$val==2||phase$val==3,1];
new$low$density$eos$compo=MapAt[switch$low$density$phase$fun,impt$low$density$eos$compo,{All,4}];

switch$high$density$phase$fun[phase$val_]:=If[phase$val==4,3];
new$high$density$eos$compo=MapAt[switch$high$density$phase$fun,impt$high$density$eos$compo,{All,4}];

Export["hs_iuf_low_density_eos_compo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_low_density_iuf_eos_compo_for_qmc_rmf_"<>ToString[rmf$model]<>".dat",new$low$density$eos$compo];

Export["high_density_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_high_density_eos_compo.dat",new$high$density$eos$compo];
];


(* ::Input::Initialization:: *)
phase$change$loop$fun:=Module[{},Do[Do[Do[change$phase$index$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}];
]


(* ::Input::Initialization:: *)
merge$low$mixed$and$high$density$eos$compo$tables$fun[temp$val_,xp$val_,rmf$model_]:=Module[{impt$low$density$eos$compo,impt$high$density$eos$compo,eos$compo,impt$single$t$xp$eos$compo$tab,density$gap$boundaries,i,density$gap$tab,n$num$den$of$nb$crust$tab,n$num$den$of$nb$core$tab,n$num$den$of$nb$mixed$tab,n$num$den$mixed$tab,mixed$eos$compo$tab},

impt$low$density$eos$compo=Import["hs_iuf_low_density_eos_compo/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_low_density_iuf_eos_compo_for_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

impt$high$density$eos$compo=Import["high_density_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_high_density_eos_compo.dat"];

density$gap$boundaries={};
If[impt$high$density$eos$compo[[1,2]]-impt$low$density$eos$compo[[-1,2]]>1,
AppendTo[density$gap$boundaries,{impt$low$density$eos$compo[[-1,2]],impt$high$density$eos$compo[[1,2]]}]];

If[Length[density$gap$boundaries]==0,eos$compo=Join[impt$low$density$eos$compo,impt$high$density$eos$compo],

density$gap$tab=Table[i,{i,density$gap$boundaries[[1,1]]+1,density$gap$boundaries[[1,2]]-1}];

n$num$den$of$nb$crust$tab=Table[{nb$tab[[impt$low$density$eos$compo[[i,2]]]],impt$low$density$eos$compo[[i,9]]},{i,1,Length[impt$low$density$eos$compo]}];
n$num$den$of$nb$core$tab=Table[{nb$tab[[impt$high$density$eos$compo[[i,2]]]],impt$high$density$eos$compo[[i,9]]},{i,1,Length[impt$high$density$eos$compo]}];

n$num$den$of$nb$mixed$tab=Table[{nb$tab[[density$gap$tab[[i]]]],Interpolation[Join[n$num$den$of$nb$crust$tab,n$num$den$of$nb$core$tab],InterpolationOrder->1][nb$tab[[density$gap$tab[[i]]]]]},{i,1,Length[density$gap$tab]}];
n$num$den$mixed$tab=Table[n$num$den$of$nb$mixed$tab[[i,2]],{i,1,Length[n$num$den$of$nb$mixed$tab]}];

mixed$eos$compo$tab=Table[{impt$low$density$eos$compo[[i,1]],density$gap$tab[[i]],impt$low$density$eos$compo[[i,3]],2,2,0,xp$val,10,n$num$den$mixed$tab[[i]],0},{i,1,Length[density$gap$tab]}];

eos$compo=Join[impt$low$density$eos$compo,mixed$eos$compo$tab,impt$high$density$eos$compo];
];

Export["combined_single_t_xp_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_eos_compo.dat",eos$compo];
]


(* ::Input::Initialization:: *)
Do[Do[Do[merge$low$mixed$and$high$density$eos$compo$tables$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
combine$diff$xp$eos$compo$fun[temp$val_,rmf$model_]:=Module[{diff$xp$tab},

diff$xp$tab=Flatten[Table[Import["combined_single_t_xp_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$tab[[xp]]]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_eos_compo.dat"],{xp,1,Length[xp$tab]}],1];

Export["combined_single_t_xp_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_compo.dat",diff$xp$tab];

Print["Done with T = "<>ToString[temp$val]<>" MeV for QMC-RMF"<>ToString[rmf$model]<>" combine t xp eos.compo"];
]


(* ::Input::Initialization:: *)
Do[Do[combine$diff$xp$eos$compo$fun[temp$tab[[t]],rmf$num],{t,1,Length[temp$tab]}],{rmf$num,1,4}]


(* ::Input::Initialization:: *)
combine$diff$t$eos$compo$fun[rmf$model_]:=Module[{diff$t$tab},

diff$t$tab=Flatten[Table[Import["combined_single_t_xp_eos_compo_files/t_"<>ToString[temp$tab[[t]]]<>"_mev_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_compo.dat"],{t,1,Length[temp$tab]}],1];

Export["combined_final_eos_compo_files/all_t_all_xp_qmc_rmf_"<>ToString[rmf$model]<>"_eos_compo.dat",diff$t$tab];

Print["Done making eos.compo for QMC-RMF"<>ToString[rmf$model]<>""];
]


(* ::Input::Initialization:: *)
Do[combine$diff$t$eos$compo$fun[rmf$num],{rmf$num,1,4}]
