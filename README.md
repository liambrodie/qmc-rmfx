This is a set of files to generate the QMC-RMF equation of state tables hosted on the CompOSE equation of state database: https://compose.obspm.fr/eos/297 https://compose.obspm.fr/eos/298 https://compose.obspm.fr/eos/299 https://compose.obspm.fr/eos/300 .

Please run the codes in the following order. Within a particular step, the code can be run parallel in temperature (T) and proton fraction (xp). The only code that cannot be run in parallel is combining_high_and_low_density_phases.m. The files finite_temperature_iuf_type_mean_field_solver.m and mean_fields_at_iuf_crust_points_fun.m do not need to be executed. All codes can be run as is in your home directory. Otherwise, you will have to change the file paths. 

1) solve_mean_fields_at_iuf_crust_points.m (Parallelization recommended)

2) comparing_phases_hs_iuf_and_qmc_rmf.m

3) generating_low_density_hs_iuf_tables.m

4) compose_high_density_eos_compo_table_generator.m

5) compose_high_density_eos_micro_table_generator.m

6) compose_high_density_eos_thermo_table_generator.m (Parallelization recommended)

7) combining_high_and_low_density_phases.m
