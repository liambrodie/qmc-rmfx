(* ::Package:: *)

(* ::Input:: *)
(*(*for calculating eos.compo at finite temperature and out of equilibrium*)*)


(* ::Input:: *)
(*(*Liam Brodie: December 2022*)*)


(* ::Input::Initialization:: *)
Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];


(* ::Input::Initialization:: *)
sigma$mass=491.5;(*meson masses used in IUF paper: arxiv: 1008.3030v1*)
omega$mass=782.5;
rho$mass=763.0;
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = 0.16 * hbarc^3;(*MeV^3*)

sigma$field$guess=30; (*initial choices for FindRoot*)
omega$field$guess=20;
rho$field$guess = 0.0000001;
sigma$coupling$guess =10;
omega$coupling$guess =10;
proton$chemical$potential$guess = 980;
neutron$chemical$potential$guess = 980;
electron$chemical$potential$guess=120;

couplings$qmc$rmft=Import["qmc_rmf_coupling_list.hdf",{{"Datasets"},{"Dataset1"}}];


(* ::Input::Initialization:: *)
temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)
xp$tab$impt=Import["iuf_data/hs_iuf_xp.dat"];
xp$tab=Table[xp$tab$impt[[i,1]],{i,1+2,Length[xp$tab$impt]}];(*to get rid of the header*)
nb$fm3$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nb$tab=Table[nb$fm3$tab$impt[[i,1]],{i,1+2,Length[nb$fm3$tab$impt]}];(*to get rid of the header*)



(* ::Subsubsection::Initialization:: *)
(*(*(*Calculating composition quantities at finite temperature out of equilibrium for CompOSE*)*)*)


(* ::Input::Initialization:: *)
(*generate an interpolating function of nb(mu_z) [fm^-3]*)
nbfm3$of$muz$fun[temp$val_,xp$val_,rmf$model_,muz$val_]:=Module[{impt$crust$tab,impt$rmf$tab,crust$nbfm3$of$muz$tab,crust$nbfm3$of$muz$interp,muz$trans$val,rmf$nbfm3$of$muz$interp},

impt$rmf$tab=Import["nbfm3_of_muz_qmc_rmf_"<>ToString[rmf$model]<>"/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_nbfm3_of_muz.dat"];
muz$trans$val=Import["muz_trans_qmc_rmf_"<>ToString[rmf$model]<>"/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_muz_trans_crust_core_pressure_val.dat"][[1,1]];

rmf$nbfm3$of$muz$interp[muz_]:=Interpolation[impt$rmf$tab,InterpolationOrder->1][muz];

rmf$nbfm3$of$muz$interp[muz$val]
];


(* ::Input::Initialization:: *)
high$density$eos$compo$fun[temp$val_,xp$val_,rmf$model_]:=Module[{impt$rmf$mean$fields,bag$const$impt,i,rmf$mean$fields$one$xp,muz$trans$val,qmc$rmf$neutron$mass,high$density$eos$compo$tab,begin$density$index$val,neutron$fraction$tab,proton$fraction$tab,electron$fraction$tab,begin$density$val},
qmc$rmf$neutron$mass=939;(*mev*)
impt$rmf$mean$fields=Import["qmc_rmf_mean_field_solns/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

muz$trans$val=Import["muz_trans_qmc_rmf_"<>ToString[rmf$model]<>"/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_muz_trans_crust_core_pressure_val.dat"][[1,1]];

(*select the qmc-rmf mean-field solns when mu_z > mu_z transition*)
rmf$mean$fields$one$xp=Select[impt$rmf$mean$fields,#1[[-1]]==xp$val&&((1-#1[[16]])*#1[[11]]+#1[[16]]*(#1[[12]]+#1[[13]]))>muz$trans$val&];

bag$const$impt=Import["bag_consts_qmc_rmfs.dat"][[rmf$model]][[1]];

begin$density$index$val =
  Position[impt$rmf$mean$fields, rmf$mean$fields$one$xp[[1]]][[1,1]] + (306-Length[impt$rmf$mean$fields]);
(*306 is the length of the density table taken up to 10 n0*)

neutron$fraction$tab=Table[rmf$mean$fields$one$xp[[i,14]](1-rmf$mean$fields$one$xp[[i,16]])/rmf$mean$fields$one$xp[[i,14]],{i,1,Length[rmf$mean$fields$one$xp]}];

proton$fraction$tab=Table[rmf$mean$fields$one$xp[[i,16]],{i,1,Length[rmf$mean$fields$one$xp]}];

electron$fraction$tab=proton$fraction$tab;

(*CompOSE particle index values are hardcoded in*)
high$density$eos$compo$tab=Table[{Position[temp$tab,rmf$mean$fields$one$xp[[i,15]]][[1,1]],begin$density$index$val+(i-1),Position[xp$tab,rmf$mean$fields$one$xp[[i,16]]][[1,1]],3,3,0,electron$fraction$tab[[i]],10,neutron$fraction$tab[[i]],11,proton$fraction$tab[[i]],0},{i,1,Length[rmf$mean$fields$one$xp]}];

Export["high_density_eos_compo_files/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>"_high_density_eos_compo.dat",high$density$eos$compo$tab];

Print["Done with t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>" eos compo"]
];

(*t$start=1 ;t$end=81 ;
xp$start= 1 ;xp$end= 60 ;*)

Do[Do[Do[high$density$eos$compo$fun[temp$tab[[t]],xp$tab[[xp]],rmf$num],{t,1,Length[temp$tab]}],{xp,1,Length[xp$tab]}],{rmf$num,1,4}]


