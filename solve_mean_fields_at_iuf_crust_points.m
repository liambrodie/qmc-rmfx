(* ::Package:: *)

(*This file executes mean_fields_at_iuf_crust_points_fun.m to solve the RMF eqs of motion at the IUF grid points and checks that the solutions 
are "good" when plugged back into the eqs of motion*)


(* ::Input::Initialization:: *)
Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];
Needs["solve$mean$fields$at$hs$iuf$crust$nb$xp`","mean_fields_at_iuf_crust_points_fun.m"];


(* ::Input::Initialization:: *)
sigma$mass=491.5;(*meson masses used in IUF paper: arxiv: 1008.3030v1*)
omega$mass=782.5;
rho$mass=763.0;
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = fm3$baryon$saturation$density* hbarc^3;(*MeV^3*)

(*sigma$field$guess=40; (*initial choices for FindRoot*)
omega$field$guess=30;
rho$field$guess = 0.0001;
neutron$chemical$potential$guess = 900(*950*);
proton$chemical$potential$guess = 850(*950*);
electron$chemical$potential$guess=120(*120*);
*)



(* ::Input::Initialization:: *)
temp$tab$impt=Import["iuf_data/hs_iuf_t.dat"];
temp$tab=Table[temp$tab$impt[[i,1]],{i,1+2,Length[temp$tab$impt]}];(*to get rid of the header*)
xp$tab$impt=Import["iuf_data/hs_iuf_xp.dat"];
xp$tab=Table[xp$tab$impt[[i,1]],{i,1+2,Length[xp$tab$impt]}];
nb$tab$impt=Import["iuf_data/hs_iuf_nb.dat"];
nbfm3$tab=Table[nb$tab$impt[[i,1]],{i,1+2,Length[nb$tab$impt]}];(*to get rid of the header*)


(* ::Input::Initialization:: *)
impt$coups=Import["qmc_rmf_coupling_list.hdf",{{"Datasets"},{"Dataset1"}}];


(* ::Input::Initialization:: *)
rmf$solver$fun[temp$val_,xp$val_,rmf$model_]:=Module[{sigma$coupling$var,omega$coupling$var,rho$coupling$var,b$var,c$var,a$zeta$var,big$lambda$var,tab$for$check$impt,check$tab},
sigma$coupling$var=impt$coups[[rmf$model,1]];
omega$coupling$var=impt$coups[[rmf$model,2]];
rho$coupling$var=impt$coups[[rmf$model,3]];
b$var=impt$coups[[rmf$model,4]];
c$var=impt$coups[[rmf$model,5]];
a$zeta$var=impt$coups[[rmf$model,6]];
big$lambda$var=impt$coups[[rmf$model,7]];

mf$solver[temp$val,xp$val,rmf$model];(*solves the RMF eqs of motion*)

tab$for$check$impt=Import["qmc_rmf_mean_field_solns/t_"<>ToString[temp$val]<>"_mev_xp_"<>ToString[xp$val]<>"_qmc_rmf_"<>ToString[rmf$model]<>".dat"];

(*checks the solutions*)
check$tab=Table[{i,tab$for$check$impt[[i,14]]/hbarc^3,tab$for$check$impt[[i,15]],tab$for$check$impt[[i,16]],check$of$mean$field$solver$gen$p$frac[tab$for$check$impt[[i,1]],tab$for$check$impt[[i,2]],tab$for$check$impt[[i,3]],tab$for$check$impt[[i,4]],tab$for$check$impt[[i,5]],tab$for$check$impt[[i,6]],tab$for$check$impt[[i,7]],tab$for$check$impt[[i,8]],tab$for$check$impt[[i,9]],tab$for$check$impt[[i,10]],tab$for$check$impt[[i,11]],tab$for$check$impt[[i,12]],tab$for$check$impt[[i,13]],tab$for$check$impt[[i,14]],tab$for$check$impt[[i,15]],tab$for$check$impt[[i,16]]]},{i,1,Length[tab$for$check$impt]}];

Print[check$tab]
];


(* ::Input::Initialization:: *)
(*t$start= 1 ;t$stop= t$start ;*)

Do[Do[rmf$solver$fun[temp$tab[[t]],xp$tab[[xp]],rmf$model],{xp,1,Length[xp$tab]}],{t,1,Length[temp$tab]},{rmf$model,1,4}]



