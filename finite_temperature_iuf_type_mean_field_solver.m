(* ::Package:: *)

(*Liam Brodie: May 2022*)

(*Solves (at finite temperature) the field equations and computes some thermodynamic quantities for an IUF-type relativistic mean field model*)

(*Needs["finite$t$mean$field$solver$iuf$type`","finite_temperature_iuf_type_mean_field_solver.m"];*)


BeginPackage["finite$t$mean$field$solver$iuf$type`" ];


(*list publicly visible functions*)

u$n;
u$anti$n;
u$p;
u$anti$p;
effective$nucleon$mass;
neutron$energy;
proton$energy;
electron$energy;

neutron$fermi$momentum;
proton$fermi$momentum;
electron$fermi$momentum;

neutron$fermi$energy;
proton$fermi$energy;
electron$fermi$energy;

fermi$dirac$electron;
fermi$dirac$neutron;
fermi$dirac$anti$neutron;
fermi$dirac$proton;
fermi$dirac$anti$proton;
fermi$dirac$electron;
fermi$dirac$positron;

neutron$number$density;
proton$number$density;
electron$number$density;
photon$number$density;

scalar$number$density$n;
scalar$number$density$p;
scalar$number$density$np;

meson$lagrangian;

meson$partial$pressure;
neutron$partial$pressure;
neutron$partial$pressure$zero$temp;
np$partial$pressure;
np$partial$pressure$zero$temp;
electron$partial$pressure;
electron$partial$pressure$zero$temp;

pressure$n;
pressure$np;
pressure$npe;
pressure$npe$photons;

photon$partial$pressure;
photon$partial$entropy$density;
photon$partial$energy$density;

energy$density$npe;
energy$density$npe$photons;
direct$energy$density$npe$photons;

entropy$density$n;
entropy$density$npe$photons;
entropy$density$npe;


sigma$eom$pnm;
omega$eom$pnm;
rho$eom$pnm;
sigma$eom;
omega$eom;
rho$eom;

mean$field$solver$pnm;
sigma$field$solution$pnm;
omega$field$solution$pnm;
rho$field$solution$pnm;
neutron$chemical$potential$solution$pnm;
check$of$mean$field$solver$pnm;
mean$field$solver$pnm$save$fields;

mean$field$solver$snm;
sigma$field$solution$snm;
omega$field$solution$snm;
rho$field$solution$snm;
neutron$chemical$potential$solution$snm;
proton$chemical$potential$solution$snm;
electron$chemical$potential$solution$snm;
check$of$mean$field$solver$snm;
mean$field$solver$snm$save$fields;

mean$field$solver$beta$eq;
sigma$field$solution$beta$eq;
omega$field$solution$beta$eq;
rho$field$solution$beta$eq;
proton$chemical$potential$solution$beta$eq;
neutron$chemical$potential$solution$beta$eq;
electron$chemical$potential$solution$beta$eq;
check$of$mean$field$solver$beta$eq;
update$field$guesses$beta$eq;
mean$field$solver$beta$eq$save$fields;

mean$field$solver$gen$p$frac;
check$of$mean$field$solver$gen$p$frac;
mean$field$solver$gen$p$frac$save$fields;
sigma$field$solution$gen$p$frac;
omega$field$solution$gen$p$frac;
rho$field$solution$gen$p$frac;
neutron$chemical$potential$solution$gen$p$frac;
proton$chemical$potential$solution$gen$p$frac;
electron$chemical$potential$solution$gen$p$frac;

mean$field$solver$gen$mu;
check$of$mean$field$solver$gen$mu;
mean$field$solver$gen$mu$save$fields;


(* ::Subsection::Initialization:: *)
(*Constants*)


(* ::Input::Initialization:: *)
sigma$mass=491.5;(*MeV*) (*meson masses used in IU-FSU (aka IUF): arxiv: 1008.3030v1*)
omega$mass=782.5;(*MeV*)
rho$mass=763.0;(*MeV*)
nucleon$mass = 939; (*MeV*)
electron$mass = 0.511; (*MeV*)
hbarc = 197.3;(*fm MeV*)
fm3$baryon$saturation$density = 0.16; (*fm^-3*)
baryon$saturation$density = 0.16 * hbarc^3;(*MeV^3*)

sigma$field$guess=30; (*MeV*)(*initial choices for FindRoot*)
omega$field$guess=20;(*MeV*)
rho$field$guess = 0.0000001;(*MeV*)
sigma$coupling$guess =10;
omega$coupling$guess =10;
proton$chemical$potential$guess = 980;(*MeV*)
neutron$chemical$potential$guess = 980;(*MeV*)
electron$chemical$potential$guess=120;(*MeV*)



(*Variables*)
sigma$field;
omega$field;
rho$field;
neutron$chemical$potential;
proton$chemical$potential;
electron$chemical$potential;


Begin["`Private`"];


(* ::Subsubsection:: *)
(*Thermodynamic functions*)


(* ::Input::Initialization:: *)
u$n[omega$field_,rho$field_,omega$coupling_,rho$coupling_]:=omega$coupling*omega$field-1/2*rho$coupling*rho$field;(*neutron energy shift due to interaction*)


(* ::Input::Initialization:: *)
u$anti$n[omega$field_,rho$field_,omega$coupling_,rho$coupling_]:=omega$coupling*omega$field+1/2*rho$coupling*rho$field;(*neutron energy shift due to interaction*)(*sign of I_{3n} is flipped for anti neutrons*)


(* ::Input::Initialization:: *)
u$p[omega$field_,rho$field_,omega$coupling_,rho$coupling_]:=omega$coupling*omega$field+1/2*rho$coupling*rho$field;(*proton energy shift due to interaction*)


(* ::Input::Initialization:: *)
u$anti$p[omega$field_,rho$field_,omega$coupling_,rho$coupling_]:=omega$coupling*omega$field-1/2*rho$coupling*rho$field;(*proton energy shift due to interaction*)


(* ::Input::Initialization:: *)
effective$nucleon$mass[sigma$field_,sigma$coupling_]:=nucleon$mass - sigma$coupling*sigma$field


(* ::Input::Initialization:: *)
neutron$energy[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,k_]:=Sqrt[k^2+effective$nucleon$mass[sigma$field,sigma$coupling]^2];(*neutron single-particle energy*)


(* ::Input::Initialization:: *)
proton$energy[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,k_]:=Sqrt[k^2+effective$nucleon$mass[sigma$field,sigma$coupling]^2];(*proton single-particle energy*)


(* ::Input::Initialization:: *)
electron$energy[k_]:=Sqrt[k^2+electron$mass^2];(*electron single-particle energy*)


(* ::Input::Initialization:: *)
neutron$fermi$energy[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,neutron$chemical$potential_]:=neutron$chemical$potential-u$n[omega$field,rho$field,omega$coupling,rho$coupling]


(* ::Input::Initialization:: *)
proton$fermi$energy[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,proton$chemical$potential_]:=proton$chemical$potential-u$p[omega$field,rho$field,omega$coupling,rho$coupling]


(* ::Input::Initialization:: *)
electron$fermi$energy[electron$chemical$potential_]:=electron$chemical$potential


(* ::Input::Initialization:: *)
fermi$dirac$neutron[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,neutron$chemical$potential_,temperature_,k_]:=Module[{energy$n,eFn},
energy$n=neutron$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
eFn=neutron$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential];

1/(Exp[((energy$n)-eFn)/temperature]+1)
];


(* ::Input::Initialization:: *)
fermi$dirac$anti$neutron[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,neutron$chemical$potential_,temperature_,k_]:=Module[{energy$n,anti$n$shift},
energy$n=neutron$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
anti$n$shift=u$anti$n[omega$field,rho$field,omega$coupling,rho$coupling];

1/(Exp[((energy$n)+(neutron$chemical$potential-anti$n$shift))/temperature]+1)
];


(* ::Input::Initialization:: *)
fermi$dirac$proton[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,proton$chemical$potential_,temperature_,k_]:=Module[{energy$p,eFp},
energy$p=proton$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
eFp=proton$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential];

1/(Exp[((energy$p)-eFp)/temperature]+1)
];


(* ::Input::Initialization:: *)
fermi$dirac$anti$proton[sigma$field_,omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,proton$chemical$potential_,temperature_,k_]:=Module[{energy$p,anti$p$shift},
energy$p=proton$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
anti$p$shift=u$anti$p[omega$field,rho$field,omega$coupling,rho$coupling];

1/(Exp[((energy$p)+(proton$chemical$potential-anti$p$shift))/temperature]+1)
];


(* ::Input::Initialization:: *)
fermi$dirac$electron[electron$chemical$potential_,temperature_,k_]:=1/(Exp[(electron$energy[k]-electron$fermi$energy[electron$chemical$potential])/temperature]+1)


(* ::Input::Initialization:: *)
fermi$dirac$positron[electron$chemical$potential_,temperature_,k_]:=1/(Exp[(electron$energy[k]+electron$fermi$energy[electron$chemical$potential])/temperature]+1)


(* ::Input::Initialization:: *)
neutron$number$density[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:=2*4Pi/(2Pi)^3*(NIntegrate[k^2*fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k],{k,0,Abs[neutron$fermi$energy[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential]],Infinity},AccuracyGoal->10,PrecisionGoal->10]-NIntegrate[k^2*fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k],{k,0,Abs[neutron$chemical$potential-u$anti$n[omega$field,rho$field,omega$coupling,rho$coupling]],Infinity},AccuracyGoal->10,PrecisionGoal->10])


(* ::Input::Initialization:: *)
proton$number$density[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:=2*4Pi/(2Pi)^3*(NIntegrate[k^2*fermi$dirac$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k],{k,0,Abs[proton$fermi$energy[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential]],Infinity},AccuracyGoal->10,PrecisionGoal->10]-NIntegrate[k^2*fermi$dirac$anti$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k],{k,0,Abs[proton$chemical$potential-u$anti$p[omega$field,rho$field,omega$coupling,rho$coupling]],Infinity},AccuracyGoal->10,PrecisionGoal->10])


(* ::Input::Initialization:: *)
electron$number$density[electron$chemical$potential_?NumericQ,temperature_?NumericQ]:=2*4Pi/(2Pi)^3*(NIntegrate[k^2*fermi$dirac$electron[electron$chemical$potential,temperature,k],{k,0,Abs[electron$fermi$energy[electron$chemical$potential]],Infinity}(*,AccuracyGoal->3,PrecisionGoal->3*)]-NIntegrate[k^2*fermi$dirac$positron[electron$chemical$potential,temperature,k],{k,0,Abs[electron$fermi$energy[electron$chemical$potential]],Infinity},AccuracyGoal->10,PrecisionGoal->10])


(* ::Input::Initialization:: *)
neutron$fermi$momentum[sigma$field_, omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,neutron$chemical$potential_,temperature_]:= (3Pi^2 neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])^(1/3)


(* ::Input::Initialization:: *)
proton$fermi$momentum[sigma$field_, omega$field_,rho$field_,sigma$coupling_,omega$coupling_,rho$coupling_,proton$chemical$potential_,temperature_]:= (3Pi^2 proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature])^(1/3)


(* ::Input::Initialization:: *)
electron$fermi$momentum[electron$chemical$potential_,temperature_]:=(3Pi^2 electron$number$density[electron$chemical$potential,temperature])^(1/3)


(* ::Input::Initialization:: *)
scalar$number$density$n[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,m$eff,fd$n,kFn,int$n,fd$anti$n,int$anti$n},
m$eff=effective$nucleon$mass[sigma$field,sigma$coupling];

kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
fd$n=fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
fd$anti$n=fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];

int$n=NIntegrate[k^2 m$eff/Sqrt[k^2+m$eff^2]  * fd$n,  {k,0,kFn,10000}];
int$anti$n=NIntegrate[k^2 m$eff/Sqrt[k^2+m$eff^2]  * fd$anti$n,  {k,0,kFn,10000}];

 2 (2Pi*2)/(2Pi)^3 * (int$n+int$anti$n)
];


(* ::Input::Initialization:: *)
scalar$number$density$np[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,m$eff,fd$n,kFn,fd$p,kFp,int$n,int$p,fd$anti$n,int$anti$n,fd$anti$p,int$anti$p},
m$eff=effective$nucleon$mass[sigma$field,sigma$coupling];

kFn=neutron$fermi$momentum[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
fd$n=fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
fd$anti$n=fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];

kFp=proton$fermi$momentum[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature];
fd$p=fermi$dirac$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];
fd$anti$p=fermi$dirac$anti$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];

int$n=NIntegrate[k^2 m$eff/Sqrt[k^2+m$eff^2]  * fd$n,  {k,0,Abs[kFn],Infinity}(*,AccuracyGoal->3,PrecisionGoal->3*)];
int$anti$n=NIntegrate[k^2 m$eff/Sqrt[k^2+m$eff^2]  * fd$anti$n,  {k,0,Abs[kFn],Infinity}(*,AccuracyGoal->3,PrecisionGoal->3*)];
 int$p=NIntegrate[k^2 m$eff/Sqrt[k^2+m$eff^2]  * fd$p,  {k,0,Abs[kFp],Infinity}(*,AccuracyGoal->3,PrecisionGoal->3*)];
int$anti$p=NIntegrate[k^2 m$eff/Sqrt[k^2+m$eff^2]  * fd$anti$p,  {k,0,Abs[kFp],Infinity}(*,AccuracyGoal->3,PrecisionGoal->3*)];

 2 (2Pi*2)/(2Pi)^3 * (int$n + int$anti$n + int$p + int$anti$p)
];


(* ::Input::Initialization:: *)
photon$number$density[temperature_]:=2*1.2020569031/Pi^2*temperature^3 (*1.2... from Riemann zeta (3)*)


(* ::Input::Initialization:: *)
meson$lagrangian[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:= -(1/2)(sigma$mass*sigma$field)^2 -(b/3)nucleon$mass (sigma$coupling*sigma$field)^3 - (c/4)(sigma$coupling*sigma$field)^4 + (1/2)(omega$mass*omega$field)^2 + (1/2)(rho$mass*rho$field)^2 +a$zeta*(1/4)(omega$coupling*omega$field)^4+big$lambda*(rho$coupling*omega$coupling*rho$field*omega$field)^2


(* ::Input::Initialization:: *)
photon$partial$pressure[temperature_]:=Pi^2/45*temperature^4;


(* ::Input::Initialization:: *)
meson$partial$pressure[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:= meson$lagrangian[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda]


(* ::Input::Initialization:: *)
neutron$partial$pressure[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,T,e$n,kFn,eFn,int$n,eF$anti$n},
T=temperature;
e$n=neutron$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
eFn=neutron$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential];
eF$anti$n=neutron$chemical$potential-u$anti$n[omega$field,rho$field,omega$coupling,rho$coupling];

int$n = NIntegrate[k^2(Log[1+Exp[-(e$n-eFn)/T]]+Log[1+Exp[-(e$n+eF$anti$n)/T]]),  {k,0,kFn,10000}];

  2T (2Pi*2)/(2Pi)^3 (int$n)
];


(* ::Input::Initialization:: *)
np$partial$pressure[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,T,e$n,kFn,eFn,int$n,e$p,kFp,eFp,int$p,eF$anti$n,eF$anti$p},
T=temperature;
e$n=neutron$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
eFn=neutron$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential];
eF$anti$n=neutron$chemical$potential-u$anti$n[omega$field,rho$field,omega$coupling,rho$coupling];
int$n = NIntegrate[k^2(Log[1+Exp[-(e$n-eFn)/T]]+Log[1+Exp[-(e$n+eF$anti$n)/T]]),  {k,0,kFn,10000}];

e$p=proton$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,k];
kFp=proton$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature];
eFp=proton$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential];
eF$anti$p=proton$chemical$potential-u$anti$p[omega$field,rho$field,omega$coupling,rho$coupling];
int$p = NIntegrate[k^2(Log[1+Exp[-(e$p-eFp)/T]]+Log[1+Exp[-(e$p+eF$anti$p)/T]]),  {k,0,Re[kFp],10000}];

  2T (2Pi*2)/(2Pi)^3 (int$n+int$p)
];


(* ::Input::Initialization:: *)
electron$partial$pressure[electron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,T,e$e,kFe,eFe,int$e,int$p},
T=temperature;
e$e=electron$energy[k];
kFe=electron$fermi$momentum[electron$chemical$potential,temperature];
eFe=electron$fermi$energy[electron$chemical$potential];
int$e = NIntegrate[k^2(Log[1+Exp[-(e$e-eFe)/T]]),  {k,0,kFe,10000}];
int$p=NIntegrate[k^2(Log[1+Exp[-(e$e+eFe)/T]]),  {k,0,kFe,10000}];(*positron contribution*)

  2T (2Pi*2)/(2Pi)^3 (int$e+int$p)
];


(* ::Input::Initialization:: *)
pressure$n[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:=meson$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda]+neutron$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]


(* ::Input::Initialization:: *)
pressure$np[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:= meson$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda] + np$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,proton$chemical$potential,temperature]


(* ::Input::Initialization:: *)
pressure$npe[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,electron$chemical$potential_?NumericQ,temperature_?NumericQ]:= meson$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda] + np$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,proton$chemical$potential,temperature] + electron$partial$pressure[electron$chemical$potential,temperature]


(* ::Input::Initialization:: *)
pressure$npe$photons[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,electron$chemical$potential_?NumericQ,temperature_?NumericQ]:= meson$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda] + np$partial$pressure[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,proton$chemical$potential,temperature] + electron$partial$pressure[electron$chemical$potential,temperature]+photon$partial$pressure[temperature]


(* ::Input::Initialization:: *)
photon$partial$energy$density[temperature_]:=Pi^2/15*temperature^4;


(* ::Input::Initialization:: *)
energy$density$npe$photons[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,electron$chemical$potential_?NumericQ,temperature_?NumericQ] :=
-pressure$npe[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,electron$chemical$potential,temperature]+neutron$chemical$potential*neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]+proton$chemical$potential*proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+electron$chemical$potential*electron$number$density[electron$chemical$potential,temperature]+temperature*entropy$density$npe(*$photons*)[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,proton$chemical$potential,electron$chemical$potential,temperature]+photon$partial$energy$density[temperature]


(* ::Input::Initialization:: *)
direct$energy$density$npe$photons[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,electron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,kFn,kFp,kFe,m$eff,m$e,energy$n,energy$p,energy$e,fd$n,fd$p,fd$e,int$n,int$p,int$e,edens$n,edens$p,edens$e,m$s,m$w,m$r,meson$edens,photon$edens,fd$anti$n,fd$anti$p,fd$pos,int$anti$n,int$anti$p,int$pos,edens$anti$n,edens$anti$p,edens$pos},
m$s=sigma$mass;
m$w=omega$mass;
m$r=rho$mass;

m$eff=effective$nucleon$mass[sigma$field,sigma$coupling];
kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
energy$n[k_]:=Sqrt[k^2+m$eff^2];
fd$n[k_]:=fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
int$n=NIntegrate[k^2*energy$n[k]*fd$n[k],{k,0,Abs[kFn],10000},AccuracyGoal->5,PrecisionGoal->5];
edens$n=2*(2*2Pi)/(2Pi)^3*int$n;

fd$anti$n[k_]:=fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
int$anti$n=NIntegrate[k^2*energy$n[k]*fd$anti$n[k],{k,0,Abs[kFn],10000},AccuracyGoal->5,PrecisionGoal->5];
edens$anti$n=2*(2*2Pi)/(2Pi)^3*int$anti$n;

m$eff=effective$nucleon$mass[sigma$field,sigma$coupling];
kFp=proton$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature];
energy$p[k_]:=Sqrt[k^2+m$eff^2];
fd$p[k_]:=fermi$dirac$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];
int$p=NIntegrate[k^2*energy$p[k]*fd$p[k],{k,0,Abs[kFp],10000},AccuracyGoal->5,PrecisionGoal->5];
edens$p=2*(2*2Pi)/(2Pi)^3*int$p;

fd$anti$p[k_]:=fermi$dirac$anti$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];
int$anti$p=NIntegrate[k^2*energy$p[k]*fd$anti$p[k],{k,0,Abs[kFp],10000},AccuracyGoal->5,PrecisionGoal->5];
edens$anti$p=2*(2*2Pi)/(2Pi)^3*int$anti$p;

m$e=electron$mass;
kFe=electron$fermi$momentum[electron$chemical$potential,temperature];
energy$e[k_]:=Sqrt[k^2+m$e^2];
fd$e[k_]:=fermi$dirac$electron[electron$chemical$potential,temperature,k];
int$e=NIntegrate[k^2*energy$e[k]*fd$e[k],{k,0,Abs[kFe],10000},AccuracyGoal->5,PrecisionGoal->5];
edens$e=2*(2*2Pi)/(2Pi)^3*int$e;

fd$pos[k_]:=fermi$dirac$positron[electron$chemical$potential,temperature,k];
int$pos=NIntegrate[k^2*energy$e[k]*fd$pos[k],{k,0,Abs[kFe],10000},AccuracyGoal->5,PrecisionGoal->5];
edens$pos=2*(2*2Pi)/(2Pi)^3*int$pos;

meson$edens=
(1/2)sigma$mass^2*sigma$field^2+(b*nucleon$mass/3)(sigma$coupling*sigma$field)^3+(c/4)(sigma$coupling*sigma$field)^4+(1/2)omega$mass^2*omega$field^2+(1/2)rho$mass^2*rho$field^2+(3*a$zeta/4)(omega$coupling*omega$field)^4+3*big$lambda*(omega$coupling*rho$coupling*omega$field*rho$field)^2;

photon$edens=photon$partial$energy$density[temperature];

edens$n+edens$anti$n+edens$p+edens$anti$p+edens$e+edens$pos+meson$edens+photon$edens

];


(* ::Input::Initialization:: *)
photon$partial$entropy$density[temperature_]:=4Pi^2/45*temperature^3;


(* ::Input::Initialization:: *)
entropy$density$n[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,kFn,fd$n,fd$anti$n},
kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
fd$n=fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
fd$anti$n=fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];

-2*(2*2Pi)/(2Pi)^3*(NIntegrate[k^2*((1-fd$n)Log[1-fd$n]+fd$n*Log[fd$n]),{k,0,kFn,10000},Method->"AdaptiveQuasiMonteCarlo"]+NIntegrate[k^2*((1-fd$anti$n)Log[1-fd$anti$n]+fd$anti$n*Log[fd$anti$n]),{k,0,kFn,10000},Method->"AdaptiveQuasiMonteCarlo"])

]


(* ::Input::Initialization:: *)
entropy$density$npe[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,electron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,kFn,fd$n,int$n,kFp,fd$p,int$p,kFe,fd$e,int$e,fd$pos,int$pos,eFn,n$fun,eFp,eFe,p$fun,e$fun,pos$fun,m$eff,a,b,c,a$val,fd$anti$n,fd$anti$p,eF$anti$n,eF$anti$p,int$anti$n,int$anti$p},
kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
fd$n=fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
fd$anti$n=fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];

kFp=proton$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature];
fd$p=fermi$dirac$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];
fd$anti$p=fermi$dirac$anti$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];

kFe=electron$fermi$momentum[electron$chemical$potential,temperature];
fd$e=fermi$dirac$electron[electron$chemical$potential,temperature,k];
fd$pos=fermi$dirac$positron[electron$chemical$potential,temperature,k];

eFn=neutron$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential];
eF$anti$n=neutron$chemical$potential-u$anti$n[omega$field,rho$field,omega$coupling,rho$coupling];
eFp=proton$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential];
eF$anti$p=proton$chemical$potential-u$anti$p[omega$field,rho$field,omega$coupling,rho$coupling];
eFe=electron$fermi$energy[electron$chemical$potential];

m$eff=effective$nucleon$mass[sigma$field,sigma$coupling];

(*Fermi-Dirac is Taylor expanded when argument is small*)

a=10^-12;
b=20;
c=20;
n$fun[k_?NumericQ]:=Module[{x,fd},
x=((Sqrt[k^2+m$eff^2]-eFn)/temperature);

fd=1/(1+Exp[x]);

Piecewise[{{((1)/(x+2))Log[(1)/(x+2)]+((x+1)/(x+2))Log[((x+1)/(x+2))],-a<x<a},{0,x>b||x<-c}},(1-fd)Log[1-fd]+fd*Log[fd]
]
];

p$fun[k_?NumericQ]:=Module[{x,fd},
x=((Sqrt[k^2+m$eff^2]-eFp)/temperature);

fd=1/(1+Exp[x]);

Piecewise[{{((1)/(x+2))Log[(1)/(x+2)]+((x+1)/(x+2))Log[((x+1)/(x+2))],-a<x<a},{0,x>b||x<-c}},(1-fd)Log[1-fd]+fd*Log[fd]
]
];

e$fun[k_?NumericQ]:=Module[{x,fd},
x=((Sqrt[k^2+electron$mass^2]-eFe)/temperature);

fd=1/(1+Exp[x]);

Piecewise[{{((1)/(x+2))Log[(1)/(x+2)]+((x+1)/(x+2))Log[((x+1)/(x+2))],-a<x<a},{0,x>b||x<-c}},(1-fd)Log[1-fd]+fd*Log[fd]
]
];

a$val=15;
int$n=NIntegrate[k^2*n$fun[k],{k,Piecewise[{{Sqrt[(eFn-a$val*temperature)^2-m$eff^2],(eFn-a$val*temperature)>m$eff&&Sqrt[(eFn-a$val*temperature)^2-m$eff^2]>0}}],kFn-5,kFn,kFn+5,Sqrt[(eFn+a$val*temperature)^2-m$eff^2]},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$anti$n=NIntegrate[k^2*((1-fd$anti$n)Log[1-fd$anti$n]+fd$anti$n*Log[fd$anti$n]),{k,0,kFn,5000},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$p=NIntegrate[k^2*p$fun[k],{k,Piecewise[{{Sqrt[(eFp-a$val*temperature)^2-m$eff^2],(eFp-a$val*temperature)>m$eff&&Sqrt[(eFp-a$val*temperature)^2-m$eff^2]>0}}],kFp,Sqrt[(eFp+a$val*temperature)^2-m$eff^2]},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$anti$p=NIntegrate[k^2*((1-fd$anti$p)Log[1-fd$anti$p]+fd$anti$p*Log[fd$anti$p]),{k,0,kFn,5000},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$e=NIntegrate[k^2*e$fun[k],{k,Piecewise[{{Sqrt[(eFe-a$val*temperature)^2-electron$mass^2],(eFe-a$val*temperature)>electron$mass&&Sqrt[(eFe-a$val*temperature)^2-electron$mass^2]>0}}],kFe,Sqrt[(eFe+a$val*temperature)^2-electron$mass^2]},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$pos=NIntegrate[k^2*((1-fd$pos)Log[1-fd$pos]+fd$pos*Log[fd$pos]),{k,0,kFe,5000},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];(*positron contribution*)

-2*(2*2Pi)/(2Pi)^3*(int$n+int$anti$n+int$p+int$anti$p+int$e+int$pos)

]


(* ::Input::Initialization:: *)
entropy$density$npe$photons[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,electron$chemical$potential_?NumericQ,temperature_?NumericQ]:=Module[{k,kFn,fd$n,int$n,kFp,fd$p,int$p,kFe,fd$e,int$e,fd$pos,int$pos,eFn,n$fun,eFp,eFe,p$fun,e$fun,pos$fun,m$eff,a,b,c,a$val,fd$anti$n,fd$anti$p,eF$anti$n,eF$anti$p,int$anti$n,int$anti$p},
kFn=neutron$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];
fd$n=fermi$dirac$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];
fd$anti$n=fermi$dirac$anti$neutron[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature,k];

kFp=proton$fermi$momentum[sigma$field, omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature];
fd$p=fermi$dirac$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];
fd$anti$p=fermi$dirac$anti$proton[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature,k];

kFe=electron$fermi$momentum[electron$chemical$potential,temperature];
fd$e=fermi$dirac$electron[electron$chemical$potential,temperature,k];
fd$pos=fermi$dirac$positron[electron$chemical$potential,temperature,k];

eFn=neutron$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential];
eF$anti$n=neutron$chemical$potential-u$anti$n[omega$field,rho$field,omega$coupling,rho$coupling];
eFp=proton$fermi$energy[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential];
eF$anti$p=proton$chemical$potential-u$anti$p[omega$field,rho$field,omega$coupling,rho$coupling];
eFe=electron$fermi$energy[electron$chemical$potential];

m$eff=effective$nucleon$mass[sigma$field,sigma$coupling];

a=10^-12;
b=20;
c=20;
n$fun[k_?NumericQ]:=Module[{x,fd},
x=((Sqrt[k^2+m$eff^2]-eFn)/temperature);

fd=1/(1+Exp[x]);

Piecewise[{{((1)/(x+2))Log[(1)/(x+2)]+((x+1)/(x+2))Log[((x+1)/(x+2))],-a<x<a},{0,x>b||x<-c}},(1-fd)Log[1-fd]+fd*Log[fd]
]
];

p$fun[k_?NumericQ]:=Module[{x,fd},
x=((Sqrt[k^2+m$eff^2]-eFp)/temperature);

fd=1/(1+Exp[x]);

Piecewise[{{((1)/(x+2))Log[(1)/(x+2)]+((x+1)/(x+2))Log[((x+1)/(x+2))],-a<x<a},{0,x>b||x<-c}},(1-fd)Log[1-fd]+fd*Log[fd]
]
];

e$fun[k_?NumericQ]:=Module[{x,fd},
x=((Sqrt[k^2+electron$mass^2]-eFe)/temperature);

fd=1/(1+Exp[x]);

Piecewise[{{((1)/(x+2))Log[(1)/(x+2)]+((x+1)/(x+2))Log[((x+1)/(x+2))],-a<x<a},{0,x>b||x<-c}},(1-fd)Log[1-fd]+fd*Log[fd]
]
];

a$val=15;
int$n=NIntegrate[k^2*n$fun[k],{k,Piecewise[{{Sqrt[(eFn-a$val*temperature)^2-m$eff^2],(eFn-a$val*temperature)>m$eff&&Sqrt[(eFn-a$val*temperature)^2-m$eff^2]>0}}],kFn-5,kFn,kFn+5,Sqrt[(eFn+a$val*temperature)^2-m$eff^2]},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$anti$n=NIntegrate[k^2*((1-fd$anti$n)Log[1-fd$anti$n]+fd$anti$n*Log[fd$anti$n]),{k,0,kFn,5000},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$p=NIntegrate[k^2*p$fun[k],{k,Piecewise[{{Sqrt[(eFp-a$val*temperature)^2-m$eff^2],(eFp-a$val*temperature)>m$eff&&Sqrt[(eFp-a$val*temperature)^2-m$eff^2]>0}}],kFp,Sqrt[(eFp+a$val*temperature)^2-m$eff^2]},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$anti$p=NIntegrate[k^2*((1-fd$anti$p)Log[1-fd$anti$p]+fd$anti$p*Log[fd$anti$p]),{k,0,kFn,5000},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$e=NIntegrate[k^2*e$fun[k],{k,Piecewise[{{Sqrt[(eFe-a$val*temperature)^2-electron$mass^2],(eFe-a$val*temperature)>electron$mass&&Sqrt[(eFe-a$val*temperature)^2-electron$mass^2]>0}}],kFe,Sqrt[(eFe+a$val*temperature)^2-electron$mass^2]},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];
int$pos=NIntegrate[k^2*((1-fd$pos)Log[1-fd$pos]+fd$pos*Log[fd$pos]),{k,0,kFe,5000},Method->{"AdaptiveQuasiMonteCarlo",MaxPoints->10^7}];(*positron contribution*)

-2*(2*2Pi)/(2Pi)^3*(int$n+int$anti$n+int$p+int$anti$p+int$e+int$pos)+photon$partial$entropy$density[temperature]

]


(* ::Input::Initialization:: *)
sigma$eom[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:= sigma$coupling*(scalar$number$density$np[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,proton$chemical$potential,temperature])  - ( b*nucleon$mass*sigma$coupling^3*sigma$field^2 + c*sigma$coupling^4*sigma$field^3 + sigma$mass^2*sigma$field );


(* ::Input::Initialization:: *)
omega$eom[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:= Module[{nB},
 nB=(neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]+proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]);

omega$coupling*nB-a$zeta*omega$coupling^4*omega$field^3-2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field*rho$field^2-omega$mass^2 *omega$field
];


(* ::Input::Initialization:: *)
rho$eom[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ,temperature_?NumericQ]:= Module[{nI},
 nI=(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]);

rho$coupling/2*nI -2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field^2*rho$field- rho$mass^2 *rho$field
 ];


(* ::Subsection:: *)
(*PNM Functions*)


(* ::Input::Initialization:: *)
sigma$eom$pnm[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:= sigma$coupling*scalar$number$density$n[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]  - ( b*nucleon$mass*sigma$coupling^3*sigma$field^2 + c*sigma$coupling^4*sigma$field^3 + sigma$mass^2*sigma$field );


(* ::Input::Initialization:: *)
omega$eom$pnm[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:= Module[{nB},
 nB=neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];

omega$coupling*nB-a$zeta*omega$coupling^4*omega$field^3-2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field*rho$field^2-omega$mass^2 *omega$field
];


(* ::Input::Initialization:: *)
rho$eom$pnm[sigma$field_?NumericQ,omega$field_?NumericQ,rho$field_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential_?NumericQ,temperature_?NumericQ]:= Module[{nI},
 nI=-neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature];

rho$coupling/2*nI -2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field^2*rho$field- rho$mass^2 *rho$field
 ];


(* ::Input::Initialization:: *)
mean$field$solver$pnm[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= FindRoot[{

sigma$eom$pnm[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,temperature]==0,

omega$eom$pnm[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,temperature]==0,

rho$eom$pnm[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,temperature]==0,

baryon$number$density -neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]==0},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10,WorkingPrecision->MachinePrecision]


(* ::Input::Initialization:: *)
sigma$field$solution$pnm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[sigma$field/.mean$field$solver$pnm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,baryon$number$density,temperature][[1]]]


(* ::Input::Initialization:: *)
omega$field$solution$pnm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[omega$field/.mean$field$solver$pnm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,baryon$number$density,temperature][[2]]]


(* ::Input::Initialization:: *)
rho$field$solution$pnm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[rho$field/.mean$field$solver$pnm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,baryon$number$density,temperature][[3]]]


(* ::Input::Initialization:: *)
neutron$chemical$potential$solution$pnm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[neutron$chemical$potential/.mean$field$solver$pnm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,baryon$number$density,temperature][[4]]]


(* ::Input::Initialization:: *)
mean$field$solver$pnm$save$fields[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{solution$rules},solution$rules=FindRoot[{

sigma$eom$pnm[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,temperature]==0,

omega$eom$pnm[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,temperature]==0,

rho$eom$pnm[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,temperature]==0,

baryon$number$density -neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]==0},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10,WorkingPrecision->MachinePrecision];

{sigma$field/.solution$rules[[1]],omega$field/.solution$rules[[2]],rho$field/.solution$rules[[3]],neutron$chemical$potential/.solution$rules[[4]],temperature,baryon$number$density,0}
]


(* ::Input::Initialization:: *)
check$of$mean$field$solver$pnm[sigma$field$soln_?NumericQ,omega$field$soln_?NumericQ,rho$field$soln_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$soln_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{s$eq,w$eq,r$eq,nb$eq,mu$eq},

s$eq=sigma$eom$pnm[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential$soln,temperature];

w$eq=omega$eom$pnm[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$soln,temperature];

r$eq=rho$eom$pnm[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$soln,temperature];

nb$eq=baryon$number$density-(neutron$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential$soln,temperature]);

{"s$eom=",s$eq,"w$eom=",w$eq,"r$eom=",r$eq,"nb-np-nn=",nb$eq}
]


(* ::Subsection::Initialization:: *)
(*SNM Functions*)


(* ::Input::Initialization:: *)
mean$field$solver$snm[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= FindRoot[{

sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0,

proton$chemical$potential-neutron$chemical$potential==0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess},{proton$chemical$potential,proton$chemical$potential$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision]


(* ::Input::Initialization:: *)
sigma$field$solution$snm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[sigma$field/.mean$field$solver$snm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[1]]]


(* ::Input::Initialization:: *)
omega$field$solution$snm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[omega$field/.mean$field$solver$snm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[2]]]


(* ::Input::Initialization:: *)
rho$field$solution$snm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[rho$field/.mean$field$solver$snm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[3]]]


(* ::Input::Initialization:: *)
neutron$chemical$potential$solution$snm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[neutron$chemical$potential/.mean$field$solver$snm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[4]]]


(* ::Input::Initialization:: *)
proton$chemical$potential$solution$snm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[proton$chemical$potential/.mean$field$solver$snm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[5]]]


(* ::Input::Initialization:: *)
electron$chemical$potential$solution$snm[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[electron$chemical$potential/.mean$field$solver$snm[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[6]]]


(* ::Input::Initialization:: *)
mean$field$solver$snm$save$fields[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{solution$rules},solution$rules=FindRoot[{

sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0,

proton$chemical$potential-neutron$chemical$potential==0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess},{proton$chemical$potential,proton$chemical$potential$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision];


{sigma$field/.solution$rules[[1]],omega$field/.solution$rules[[2]],rho$field/.solution$rules[[3]],neutron$chemical$potential/.solution$rules[[4]],proton$chemical$potential/.solution$rules[[5]],electron$chemical$potential/.solution$rules[[6]],temperature,baryon$number$density,0.5}]


(* ::Input::Initialization:: *)
check$of$mean$field$solver$snm[sigma$field$soln_?NumericQ,omega$field$soln_?NumericQ,rho$field$soln_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$soln_?NumericQ,proton$chemical$potential$soln_?NumericQ,electron$chemical$potential$soln_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{s$eq,w$eq,r$eq,nb$eq,nq$eq,mu$eq},

s$eq=sigma$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential$soln,proton$chemical$potential$soln,temperature];

w$eq=omega$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$soln,proton$chemical$potential$soln,temperature];

r$eq=rho$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$soln,proton$chemical$potential$soln,temperature];

nb$eq=baryon$number$density-(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]+neutron$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential$soln,temperature]);

nq$eq=proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]-electron$number$density[electron$chemical$potential$soln,temperature];

mu$eq=proton$chemical$potential$soln-neutron$chemical$potential$soln;

{"s$eom=",s$eq,"w$eom=",w$eq,"r$eom=",r$eq,"nb-np-nn=",nb$eq,"np-ne=",nq$eq,"mun-mup=",mu$eq}
]


(* ::Subsection:: *)
(*Beta-equilibrium functions*)


(* ::Input::Initialization:: *)
mean$field$solver$beta$eq[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{solution$rules},

solution$rules=FindRoot[{
sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0,

neutron$chemical$potential-proton$chemical$potential-electron$chemical$potential==0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess},{proton$chemical$potential,proton$chemical$potential$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision]

]


(* ::Input::Initialization:: *)
sigma$field$solution$beta$eq[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[sigma$field/.mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[1]]]


(* ::Input::Initialization:: *)
omega$field$solution$beta$eq[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[omega$field/.mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[2]]]


(* ::Input::Initialization:: *)
rho$field$solution$beta$eq[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[rho$field/.mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[3]]]


(* ::Input::Initialization:: *)
neutron$chemical$potential$solution$beta$eq[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[neutron$chemical$potential/.mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[4]]]


(* ::Input::Initialization:: *)
proton$chemical$potential$solution$beta$eq[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[proton$chemical$potential/.mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[5]]]


(* ::Input::Initialization:: *)
electron$chemical$potential$solution$beta$eq[baryon$number$density_?NumericQ,temperature_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[electron$chemical$potential/.mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature][[6]]]


(* ::Input::Initialization:: *)
mean$field$solver$beta$eq$save$fields[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{solution$rules,charge$fraction},

solution$rules=FindRoot[{
sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0,

neutron$chemical$potential-proton$chemical$potential-electron$chemical$potential==0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess},{proton$chemical$potential,proton$chemical$potential$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision];

charge$fraction=proton$number$density[sigma$field/.solution$rules[[1]],omega$field/.solution$rules[[2]],rho$field/.solution$rules[[3]],sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential/.solution$rules[[5]],temperature]/baryon$number$density;

{sigma$field/.solution$rules[[1]],omega$field/.solution$rules[[2]],rho$field/.solution$rules[[3]],neutron$chemical$potential/.solution$rules[[4]],proton$chemical$potential/.solution$rules[[5]],electron$chemical$potential/.solution$rules[[6]],temperature,baryon$number$density,charge$fraction}

]


(* ::Input::Initialization:: *)
check$of$mean$field$solver$beta$eq[sigma$field$soln_?NumericQ,omega$field$soln_?NumericQ,rho$field$soln_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$soln_?NumericQ,proton$chemical$potential$soln_?NumericQ,electron$chemical$potential$soln_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ]:= Module[{s$eq,w$eq,r$eq,nb$eq,nq$eq,mu$eq},

s$eq=sigma$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential$soln,proton$chemical$potential$soln,temperature];

w$eq=omega$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$soln,proton$chemical$potential$soln,temperature];

r$eq=rho$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$soln,proton$chemical$potential$soln,temperature];

nb$eq=baryon$number$density-(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]+neutron$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential$soln,temperature]);

nq$eq=proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]-electron$number$density[electron$chemical$potential$soln,temperature];

mu$eq=neutron$chemical$potential$soln-proton$chemical$potential$soln-electron$chemical$potential$soln;

{"s$eom=",s$eq,"w$eom=",w$eq,"r$eom=",r$eq,"nb-np-nn=",nb$eq,"np-ne=",nq$eq,"mun-mup-mue=",mu$eq,If[s$eq>10^-6||w$eq>10^-6||r$eq>10^-6||nb$eq>10^-6||nq$eq>10^-6||mu$eq>10^-6,"WARNING: 10^-6 ACCURACY NOT ACHIEVED","Good Check"]}
]


(* ::Input::Initialization:: *)
update$field$guesses$beta$eq[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,initial$baryon$number$density_?NumericQ,final$baryon$number$density_?NumericQ,baryon$number$density$step$size_?NumericQ,temperature_?NumericQ]:=Module[{nb$table,initial$field$table,rmf$solutions,sigma$sol,omega$sol,initial$rmf$sol,rmf$solution$prev,rmf$iterator},
nb$table=Table[N[i],{i,initial$baryon$number$density,final$baryon$number$density,baryon$number$density$step$size}];(*[MeV^4]*)
initial$field$table={};
initial$rmf$sol=mean$field$solver$beta$eq[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,nb$table[[1]],temperature];

AppendTo[initial$field$table,{temperature,nb$table[[1]],sigma$field/.initial$rmf$sol,omega$field/.initial$rmf$sol,rho$field/.initial$rmf$sol,neutron$chemical$potential/.initial$rmf$sol,proton$chemical$potential/.initial$rmf$sol,electron$chemical$potential/.initial$rmf$sol}];

rmf$iterator[x_]:=mean$field$solver$beta$eq[initial$field$table[[x-1,3]],initial$field$table[[x-1,4]],initial$field$table[[x-1,5]],sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,initial$field$table[[x-1,6]],initial$field$table[[x-1,7]],initial$field$table[[x-1,8]],nb$table[[x]],temperature];

Do[

AppendTo[initial$field$table,{temperature,nb$table[[x]],sigma$field/.rmf$iterator[x],omega$field/.rmf$iterator[x],rho$field/.rmf$iterator[x],neutron$chemical$potential/.rmf$iterator[x],proton$chemical$potential/.rmf$iterator[x],electron$chemical$potential/.rmf$iterator[x]}]

,{x,2,Length[nb$table]}];

{initial$field$table}(*column labels: baryon number, sigma field, omega field, rho field*)
]


(* ::Subsubsection:: *)
(*General Proton Fraction Functions*)


(* ::Input::Initialization:: *)
check$of$mean$field$solver$gen$p$frac[sigma$field$soln_?NumericQ, omega$field$soln_?NumericQ, rho$field$soln_?NumericQ, sigma$coupling_?NumericQ, omega$coupling_?NumericQ, rho$coupling_?NumericQ, b_?NumericQ, c_?NumericQ, a$zeta_?NumericQ, big$lambda_?NumericQ, neutron$chemical$potential$soln_?NumericQ, proton$chemical$potential$soln_?NumericQ, electron$chemical$potential$soln_?NumericQ, baryon$number$density_?NumericQ, temperature_?NumericQ, proton$fraction_?NumericQ] := Module[{s$eq, w$eq, r$eq, nb$eq, nq$eq, xp$eq},
  
  s$eq = ( sigma$coupling*(scalar$number$density$np[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, proton$chemical$potential$soln, temperature])  - ( b*nucleon$mass*sigma$coupling^3*sigma$field$soln^2 + c*sigma$coupling^4*sigma$field$soln^3 + sigma$mass^2*sigma$field$soln ))/( Abs[sigma$coupling*(scalar$number$density$np[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, proton$chemical$potential$soln, temperature])]  + Abs[( b*nucleon$mass*sigma$coupling^3*sigma$field$soln^2 + c*sigma$coupling^4*sigma$field$soln^3 + sigma$mass^2*sigma$field$soln )]);
  
  w$eq = (omega$coupling*(neutron$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, temperature] + proton$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, proton$chemical$potential$soln, temperature]) - (a$zeta*omega$coupling^4*omega$field$soln^3 + 2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field$soln*rho$field$soln^2 + omega$mass^2 *omega$field$soln))/(Abs[(omega$coupling*(neutron$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, temperature] + proton$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, proton$chemical$potential$soln, temperature]))] + Abs[(a$zeta*omega$coupling^4*omega$field$soln^3 + 2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field$soln*rho$field$soln^2 + omega$mass^2 *omega$field$soln)]);
  
  r$eq = (rho$coupling/2*((proton$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, proton$chemical$potential$soln, temperature] - neutron$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, temperature])) - 2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field$soln^2*rho$field$soln - rho$mass^2 *rho$field$soln)/((rho$coupling/2*((proton$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, proton$chemical$potential$soln, temperature] - neutron$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, temperature])) + (2*big$lambda*omega$coupling^2*rho$coupling^2*omega$field$soln^2*rho$field$soln + rho$mass^2 *rho$field$soln)));
  
  nb$eq = (baryon$number$density - (proton$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, proton$chemical$potential$soln, temperature] + neutron$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, temperature]))/(baryon$number$density + (proton$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, proton$chemical$potential$soln, temperature] + neutron$number$density[sigma$field$soln, omega$field$soln, rho$field$soln, sigma$coupling, omega$coupling, rho$coupling, neutron$chemical$potential$soln, temperature]));
nq$eq=(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]-electron$number$density[electron$chemical$potential$soln,temperature])/(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]+electron$number$density[electron$chemical$potential$soln,temperature]);

xp$eq=(proton$fraction-proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]/(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]+neutron$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential$soln,temperature]))/(proton$fraction+proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]/(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential$soln,temperature]+neutron$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential$soln,temperature]));


{If[proton$fraction!=0.5,If[Abs[s$eq]>10^-2||Abs[w$eq]>10^-2||Abs[r$eq]>10^-2||Abs[nb$eq]>10^-2||Abs[nq$eq]>10^-2||Abs[xp$eq]>10^-2,Print["WARNING: 10^-3 ACCURACY NOT ACHIEVED at t="<>ToString[temperature]<>"mev xp="<>ToString[proton$fraction]<>" nb="<>ToString[baryon$number$density/hbarc^3]<>"fm-3"];{s$eq,w$eq,r$eq,nb$eq,nq$eq,xp$eq},"Good Check"],If[Abs[s$eq]>10^-2||Abs[w$eq]>10^-2||Abs[nb$eq]>10^-2||Abs[nq$eq]>10^-2||Abs[xp$eq]>10^-2,Print["WARNING: 10^-2 ACCURACY NOT ACHIEVED"];{s$eq,w$eq,nb$eq,nq$eq,xp$eq},"Good Check"]]}

]


(* ::Input::Initialization:: *)
mean$field$solver$gen$p$frac[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ]:= Module[{solns,sigma$field,omega$field,rho$field,neutron$chemical$potential,proton$chemical$potential,electron$chemical$potential,mean$field$check},

solns=FindRoot[{

sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0,

proton$fraction (proton$number$density[sigma$field, omega$field, 
      rho$field, sigma$coupling, omega$coupling, rho$coupling, 
      proton$chemical$potential, temperature] + 
     neutron$number$density[sigma$field, omega$field, rho$field, 
      sigma$coupling, omega$coupling, rho$coupling, 
      neutron$chemical$potential, temperature]) - 
  proton$number$density[sigma$field, omega$field, rho$field, 
   sigma$coupling, omega$coupling, rho$coupling, 
   proton$chemical$potential, temperature] == 0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess},{proton$chemical$potential,proton$chemical$potential$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision];

mean$field$check=check$of$mean$field$solver$gen$p$frac[sigma$field/.solns,omega$field/.solns,rho$field/.solns,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential/.solns,proton$chemical$potential/.solns,electron$chemical$potential/.solns,baryon$number$density,temperature,proton$fraction];

{"sigma$field\[Rule]",sigma$field/.solns,"omega$field\[Rule]",omega$field/.solns,"rho$field\[Rule]",rho$field/.solns,"neutron$chemcial$potential\[Rule]",neutron$chemical$potential/.solns,"proton$chemcial$potential\[Rule]",proton$chemical$potential/.solns,"electron$chemcial$potential\[Rule]",electron$chemical$potential/.solns,mean$field$check}
]


(* ::Input::Initialization:: *)
sigma$field$solution$gen$p$frac[baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[sigma$field/.mean$field$solver$gen$p$frac[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature,proton$fraction][[1]]]


(* ::Input::Initialization:: *)
omega$field$solution$gen$p$frac[baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[omega$field/.mean$field$solver$gen$p$frac[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature,proton$fraction][[2]]]


(* ::Input::Initialization:: *)
rho$field$solution$gen$p$frac[baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[rho$field/.mean$field$solver$gen$p$frac[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature,proton$fraction][[3]]]


(* ::Input::Initialization:: *)
neutron$chemical$potential$solution$gen$p$frac[baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[neutron$chemical$potential/.mean$field$solver$gen$p$frac[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature,proton$fraction][[4]]]


(* ::Input::Initialization:: *)
proton$chemical$potential$solution$gen$p$frac[baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[proton$chemical$potential/.mean$field$solver$gen$p$frac[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature,proton$fraction][[5]]]


(* ::Input::Initialization:: *)
electron$chemical$potential$solution$gen$p$frac[baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ]:=Re[electron$chemical$potential/.mean$field$solver$gen$p$frac[sigma$field$guess,omega$field$guess,rho$field$guess,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential$guess,proton$chemical$potential$guess,electron$chemical$potential$guess,baryon$number$density,temperature,proton$fraction][[6]]]


(* ::Input::Initialization:: *)
mean$field$solver$gen$p$frac$save$fields[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,neutron$chemical$potential$guess_?NumericQ,proton$chemical$potential$guess_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density_?NumericQ,temperature_?NumericQ,proton$fraction_?NumericQ]:= Module[{solns,sigma$field,omega$field,rho$field,neutron$chemical$potential,proton$chemical$potential,electron$chemical$potential,mean$field$check},

solns=FindRoot[{

sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0,

proton$fraction (proton$number$density[sigma$field, omega$field, 
      rho$field, sigma$coupling, omega$coupling, rho$coupling, 
      proton$chemical$potential, temperature] + 
     neutron$number$density[sigma$field, omega$field, rho$field, 
      sigma$coupling, omega$coupling, rho$coupling, 
      neutron$chemical$potential, temperature]) - 
  proton$number$density[sigma$field, omega$field, rho$field, 
   sigma$coupling, omega$coupling, rho$coupling, 
   proton$chemical$potential, temperature] == 0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{neutron$chemical$potential,neutron$chemical$potential$guess},{proton$chemical$potential,proton$chemical$potential$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision];

mean$field$check=check$of$mean$field$solver$gen$p$frac[sigma$field/.solns,omega$field/.solns,rho$field/.solns,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential/.solns,proton$chemical$potential/.solns,electron$chemical$potential/.solns,baryon$number$density,temperature,proton$fraction];

{sigma$field/.solns,omega$field/.solns,rho$field/.solns,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential/.solns,proton$chemical$potential/.solns,electron$chemical$potential/.solns,baryon$number$density,temperature,proton$fraction}
]


(* ::Subsection:: *)
(*General Chemical Potential Solver*)


(* ::Input::Initialization:: *)
check$of$mean$field$solver$gen$mu[sigma$field$soln_?NumericQ,omega$field$soln_?NumericQ,rho$field$soln_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,electron$chemical$potential$soln_?NumericQ,baryon$number$density$soln_?NumericQ,temperature_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ]:= Module[{s$eq,w$eq,r$eq,nb$eq,nq$eq,xp$eq},

s$eq=sigma$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature];

w$eq=omega$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature];

r$eq=rho$eom[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature];

nb$eq=baryon$number$density$soln-(proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature]);

nq$eq=proton$number$density[sigma$field$soln,omega$field$soln,rho$field$soln,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential$soln,temperature];

{If[s$eq>10^-6||w$eq>10^-6||r$eq>10^-6||nb$eq>10^-6||nq$eq>10^-6,"WARNING: 10^-6 ACCURACY NOT ACHIEVED: s$eom=,"<>ToString[s$eq]<>",w$eom=,"<>ToString[w$eq]<>",r$eom=,"<>ToString[r$eq]<>",nb-np-nn=,"<>ToString[nb$eq]<>",np-ne=,"<>ToString[nq$eq]<>"","Good Check"]}

]


(* ::Input::Initialization:: *)
mean$field$solver$gen$mu[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density$guess_?NumericQ,temperature_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ]:= Module[{solns,sigma$field,omega$field,rho$field,electron$chemical$potential,baryon$number$density,proton$fraction,mean$field$check},

solns=FindRoot[{

sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{baryon$number$density,baryon$number$density$guess},{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision
]


(* ::Input::Initialization:: *)
mean$field$solver$gen$mu$save$fields[sigma$field$guess_?NumericQ,omega$field$guess_?NumericQ,rho$field$guess_?NumericQ,sigma$coupling_?NumericQ,omega$coupling_?NumericQ,rho$coupling_?NumericQ,b_?NumericQ,c_?NumericQ,a$zeta_?NumericQ,big$lambda_?NumericQ,electron$chemical$potential$guess_?NumericQ,baryon$number$density$guess_?NumericQ,temperature_?NumericQ,neutron$chemical$potential_?NumericQ,proton$chemical$potential_?NumericQ]:= Module[{solns,sigma$field,omega$field,rho$field,electron$chemical$potential,baryon$number$density,proton$fraction,mean$field$check,charge$fraction},

solns=FindRoot[{

sigma$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

omega$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

rho$eom[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,neutron$chemical$potential,proton$chemical$potential,temperature]==0,

baryon$number$density-(proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]+neutron$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,neutron$chemical$potential,temperature])==0,

proton$number$density[sigma$field,omega$field,rho$field,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential,temperature]-electron$number$density[electron$chemical$potential,temperature]==0
},

{{sigma$field,sigma$field$guess},{omega$field,omega$field$guess},{rho$field,rho$field$guess},{baryon$number$density,baryon$number$density$guess}(*,{proton$fraction,proton$fraction$guess}*),{electron$chemical$potential,electron$chemical$potential$guess}},AccuracyGoal->10,PrecisionGoal-> 10, WorkingPrecision->MachinePrecision];

mean$field$check=check$of$mean$field$solver$gen$mu[sigma$field/.solns,omega$field/.solns,rho$field/.solns,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,electron$chemical$potential/.solns,baryon$number$density/.solns,temperature(*,proton$fraction/.solns*),neutron$chemical$potential,proton$chemical$potential];

charge$fraction=proton$number$density[sigma$field/.solns,omega$field/.solns,rho$field/.solns,sigma$coupling,omega$coupling,rho$coupling,proton$chemical$potential/.solns,temperature]/(baryon$number$density/.solns);

{"sigma$field\[Rule]",sigma$field/.solns,"omega$field\[Rule]",omega$field/.solns,"rho$field\[Rule]",rho$field/.solns,"electron$chemcial$potential\[Rule]",electron$chemical$potential/.solns,"baryon$number$density->",baryon$number$density/.solns,"at","T="<>ToString[temperature]<>" MeV","mu_n="<>ToString[neutron$chemical$potential]<>" MeV","mu_p="<>ToString[proton$chemical$potential]<>" MeV",mean$field$check};

{sigma$field/.solns,omega$field/.solns,rho$field/.solns,sigma$coupling,omega$coupling,rho$coupling,b,c,a$zeta,big$lambda,electron$chemical$potential/.solns,baryon$number$density/.solns,temperature,neutron$chemical$potential,proton$chemical$potential,charge$fraction}
]


End[]; (* end of private part of module *)
EndPackage[];
